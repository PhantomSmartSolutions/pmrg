<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pmrg' );

/** MySQL database username */
define( 'DB_USER', 'pmrg' );

/** MySQL database password */
define( 'DB_PASSWORD', 'g10b@l@dm1n@99' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x>&MJttFx-J3yIMWp;wWYB?+4JTcp)<>thGt5=LUZ^XwnxpRaS*T-_dLe5`{A,IT' );
define( 'SECURE_AUTH_KEY',  '%H^Z/|l;>QTY;Ui8p90]afdu^%O&]r!BOl>P?%yM-6Ws*SkMJmoY2nQ$^SMVSLT-' );
define( 'LOGGED_IN_KEY',    't#+6CXg9c%8lmDbWP,U4X1!2$S&Z_Bj$<M|-(?)SXJT0Kp55^Q_Fr<`)8dB#NwH~' );
define( 'NONCE_KEY',        '8949.Z{$}!DhvXT7_RiLrR;Rx5N9a*7Hzm^g0[AI ;G|P-DpyE$_Uhf RG(z%*QF' );
define( 'AUTH_SALT',        'eV/u1oIg%y4n*T5EEhb>jM|fyICl^P-lHnvS:)Z1^5TZdOWDf 9vLsrwtg$FFcrN' );
define( 'SECURE_AUTH_SALT', '5u=1d6$4L(:Xel>Qx{r|2vkZsQh=m3soNVNZ,]<2SZ<#8fvO*oLH+]5#{$MQglTR' );
define( 'LOGGED_IN_SALT',   'eAvRFU5gfNQ[}{ry4z~a>r:GD(5IcBx~q9>e89I(<&xjZ!A[1>(X]{&:C*btVpPE' );
define( 'NONCE_SALT',       'Sh1?@#_J/g%|%tXM6Zr<<j#2BwY~NDAEqK6&Dj?;P#! Qp12EQ{J,riS{Y3@{LIw' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
