<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>
<style>
	#menu-main-menu #menu-item-373 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	header.fixed #menu-main-menu #menu-item-373 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }
	.media-body ul{ margin-left: 7px; }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    

		<section>
			<div class="slider3d first">
			  <div class="slider3d__wrapper">
			    <div class="slider3d__inner">
			      <div class="slider3d__rotater">
			      	<?php

						// check if the repeater field has rows of data
						if( have_rows('banner_slider') ):

						 	// loop through the rows of data
						    while ( have_rows('banner_slider') ) : the_row();
						    	$slideimg = get_sub_field('slider_image');
						?>
			        <div class="slider3d__item" style="background-image: url('<?php echo $slideimg['url']; ?>');">
			        	<div class="slider3d-cont"><center>
			        		<h2 class="font-50" data-text="<?php the_sub_field('slider_title'); ?>"><?php the_sub_field('slider_title'); ?></h2>
					        <p class="font-20"><?php the_sub_field('slider_content'); ?></p>
					        <a href="<?php the_sub_field('slider_button_link'); ?>" class="btBtn btBtn btnFilledStyle btnAlternateColor btnMedium btnNormalWidth btnRightPosition btnNoIcon" >
					          	<span class="btnInnerText"><?php the_sub_field('slider_button_text'); ?></span>
					        </a></center>
			        	</div>
			        </div>
			        <?php

			        endwhile;

					else :

					    // no rows found

					endif;

					?>
			        <?php /*
			        <div class="slider3d__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/ban1.png');">
			        	<div class="slider3d-cont"><center>
			        		<h2 class="font-50" data-text="Ophthalmologists">Ophthalmologists</h2>
					        <p class="font-20">The Ophthalmology Billing Specialists. A different kind of medical billing service.</p>
					        <a href="#" class="btBtn btBtn btnFilledStyle btnAlternateColor btnMedium btnNormalWidth btnRightPosition btnNoIcon" >
					          	<span class="btnInnerText">Discover more</span>
					        </a></center>
			        	</div>
			        </div>
			        <div class="slider3d__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bgn-slider-05.jpg');">
			        	<div class="slider3d-cont"><center>
			        		<h2 class="font-50" data-text="Credentialing">Credentialing</h2>
				            <p class="font-20">The Ophthalmology Billing Specialists. A different kind of medical billing service.</p>
				            <a href="#" class="btBtn btBtn btnFilledStyle btnAlternateColor btnMedium btnNormalWidth btnRightPosition btnNoIcon" >
				          		<span class="btnInnerText">Discover more</span>
				            </a></center>
			        	</div>
			        </div>
			        <div class="slider3d__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bgn-slider-04.jpg');">
			        	<div class="slider3d-cont"><center>
			        		<h2 class="font-50" data-text="Other Services">Other Services</h2>
				            <p class="font-20">The Ophthalmology Billing Specialists. A different kind of medical billing service.</p>
				            <a href="#" class="btBtn btBtn btnFilledStyle btnAlternateColor btnMedium btnNormalWidth btnRightPosition btnNoIcon" >
				          		<span class="btnInnerText">Discover more</span>
				            </a></center>
			        	</div>
			        </div> */ ?>
			      </div>
			    </div>
			  </div>
			  <div class="slider3d__controls">
			    <div class="slider3d__handle">
			      <div class="slider3d__handle__inner">
			        <div class="slider3d__handle__rotater">
			          <div class="slider3d__handle__item active"><?php the_field('banner_slider_indicator_text'); ?></div>
			          <?php

						// check if the repeater field has rows of data
						if( have_rows('banner_slider_indicators_text') ):

						 	// loop through the rows of data
						    while ( have_rows('banner_slider_indicators_text') ) : the_row();
						    	
						?>
			          <div class="slider3d__handle__item"><?php the_sub_field('indicator_text'); ?></div>
			          <?php

			        endwhile;

					else :

					    // no rows found

					endif;

					?>
			          <!-- <div class="slider3d__handle__item">3</div> -->
			          <!-- <div class="slider3d__handle__item">Page 4</div>
			          <div class="slider3d__handle__item">Page 5</div> -->
			        </div>
			      </div>
			    </div>
			    <div class="slider3d__control m--up"></div>
			    <div class="slider3d__control m--down"></div>
			  </div>
			</div>
		</section>

		<?php /*
		<section class="services-sec padding-50-20 blue-grey-background">
		    <div class="container">
		   		<div class="row">
		   			<div class="col-md-2"></div>
		   			<div class="col-md-8 text-center">
		   				<p class="font-20"><?php the_field('services_tagline'); ?></p>
		   				<center><hr></center>
		   				<p class="font-50"><?php the_field('services_main_title_1'); ?> <br><span class="skyblue"><b><?php the_field('services_main_title_2'); ?></b></span></p>
		   			</div>
		   			<div class="col-md-2"></div>
		   		</div><br>
		   		<div class="home-serve-grid">
		   			<div class="">
		   				<div class="">
		   					<div class="">
		   						<div class="serve-sec">
					   				<div class="serve-cont text-center">
					   					<p><div class="serve-bar"></div><i class="fa fa-file-text-o"></i></p>
					   					<h5><b><?php the_field('services_title_1'); ?></b></h5>
					   					<p class="font-16"><?php the_field('services_content_1'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   				</div>
		   			</div>
		   			<div class="">
		   				<div class="">
		   					<div class="">
		   						<div class="serve-sec">
					   				<div class="serve-cont text-center">
					   					<p><div class="serve-bar"></div><i class="fa fa-id-card-o"></i></p>
					   					<h5><b><?php the_field('services_title_2'); ?></b></h5>
					   					<p class="font-16"><?php the_field('services_content_2'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   				</div>
		   			</div>
		   			<div class="">
		   				<div class="">
		   					<div class="">
		   						<div class="serve-sec">
					   				<div class="serve-cont text-center">
					   					<p><div class="serve-bar"></div><i class="fa fa-users"></i></p>
					   					<h5><b><?php the_field('services_title_3'); ?></b></h5>
					   					<p class="font-16"><?php the_field('services_content_3'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   		
		   </div>
		</section> */ ?>

		<section class="section padding-50-20"  >
    
        <div class="container">
            <div class="row">
		   			<div class="col-md-2"></div>
		   			<div class="col-md-8 text-center">
		   				<p class="font-17"><?php the_field('services_tagline'); ?></p>
		   				<center><hr class="hr-center"></center>
		   				<p class="font-25"><?php the_field('services_main_title_1'); ?> <span class="skyblue"><b><?php the_field('services_main_title_2'); ?></b></span></p>
		   			</div>
		   			<div class="col-md-2"></div>
		   		</div><br>

            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 mb-6">



<div class="position-relative">
    <div class="border rounded px-6 py-7 bg-light pb-11 translate-hover" >
                    <div class="mb-4">
                        <i class="icon-dollar-currency-2 text-primary fs-50"></i>
                    </div>
        <div class="media">
            <div class="media-body">
                    <!-- <p class=" o-8">Business Service</p> -->

				<a class="color-333" href="<?php the_field('service_link_1'); ?>"><h5><b><?php the_field('services_title_1'); ?></b></h5></a>
                <p class="font-16"><?php the_field('services_content_1'); ?></p>

            </div>
        </div>
    </div>

<div class="oval-divider--bottom oval-divider--8"  ></div>
</div>


                        </div>
                        <div class="col-md-4 col-lg-4 mb-6">



<div class="position-relative">
    <div class="border rounded px-6 py-7 bg-primary pb-11 translate-hover" >
                    <div class="mb-4">
                        <i class="icon-shopping-bag-3 text-white fs-50"></i>
                    </div>
        <div class="media">
            <div class="media-body">
                <a class="color-white" href="<?php the_field('service_link_2'); ?>"><h5><b><?php the_field('services_title_2'); ?></b></h5></a>
				<p class="font-16"><?php the_field('services_content_2'); ?></p>

            </div>
        </div>
    </div>

<div class="oval-divider--bottom oval-divider--8"  ></div>
</div>


                        </div>
                        <div class="col-md-4 col-lg-4 mb-6">



<div class="position-relative">
    <div class="border rounded px-6 py-7 bg-light pb-11 translate-hover" >
                    <div class="mb-4">
                        <i class="icon-window-hand text-primary fs-50"></i>
                    </div>
        <div class="media">
            <div class="media-body">
                <a class="color-333" href="<?php the_field('service_link_3'); ?>"><h5><b><?php the_field('services_title_3'); ?></b></h5></a>
				<p class="font-16"><?php the_field('services_content_3'); ?></p>

            </div>
        </div>
    </div>

<div class="oval-divider--bottom oval-divider--8"  ></div>
</div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    
</section>

		<?php
						$membimg1 = get_field('team_member_image_1');
						$membimg2 = get_field('team_member_image_2');
						$membimg3 = get_field('team_member_image_3');
						$membimg4 = get_field('team_member_image_4');
						$membimg5 = get_field('team_member_image_5');
						$membimg6 = get_field('team_member_image_6');
						$membimg7 = get_field('team_member_image_7');
						$membimg8 = get_field('team_member_image_8');
					?>

		<section class="section padding-50-20 blue-grey-background">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center><p class="font-17"><?php the_field('management_team_tagline'); ?></p>
						<hr class="hr-center">
						<p class="font-25 margin-10"><?php the_field('management_title_1'); ?> <span class="skyblue"><b><?php the_field('management_title_2'); ?></b></span></p></center>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg1['url']; ?>" alt="<?php echo $membimg1['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_1'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_1'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_1'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg2['url']; ?>" alt="<?php echo $membimg2['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_2'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_2'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_2'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg3['url']; ?>" alt="<?php echo $membimg3['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_3'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_3'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_3'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg4['url']; ?>" alt="<?php echo $membimg4['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_4'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_4'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_4'); ?></p>
						</div>
					</div>
				</div>
				<div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg5['url']; ?>" alt="<?php echo $membimg5['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_5'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_5'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_5'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg6['url']; ?>" alt="<?php echo $membimg6['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_6'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_6'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_6'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg7['url']; ?>" alt="<?php echo $membimg7['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_7'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_7'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_7'); ?></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 team-mem-col">
						<div class="team-mem-section">
							<img src="<?php echo $membimg8['url']; ?>" alt="<?php echo $membimg8['alt']; ?>">
							<div class="team-mem-overlay">
								<a href="<?php the_field('team_member_link_8'); ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						
						<div class="designation-caption text-center">
							<p class="font-20"><b><?php the_field('team_member_name_8'); ?></b></p>
							<p class="font-15"><?php the_field('team_member_designation_8'); ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php 
			$testibackimg = get_field('testimonials_background_image');
		?>

		<section class="client-testimonials padding-50-20" id="client-testimonial-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center>
							<p class="font-17"><?php the_field('testimonials_tagline'); ?></p>
							<hr class="hr-center">
							<p class="font-25"><b><?php the_field('testimonials_title_1'); ?> <span class="skyblue"><?php the_field('testimonials_title_2'); ?></span></b></p>
						</center>
						<div id="demo">

					        <div id="owl-demo" class="owl-carousel">
					        	<?php 
					    		if ( have_rows('testimonial_section') ):
					    			while ( have_rows('testimonial_section') ) : the_row();
					    				$clientimg = get_sub_field('client_image');
					    				?>
					        	<div class="item">
					          		<?php
					          			if (!empty($clientimg)) {
					          		?>
						          	<div class="img-overlay">
										<figure><img src="<?php echo $clientimg['url']; ?>" alt="<?php echo $clientimg['alt']; ?>"></figure>
									</div>
									<?php
									}
										?>
						          	<center>
						          		<p class="font-20"><?php the_sub_field('client_feedback_tagline'); ?></p>
						          		<hr class="hr-center">
						          		<p><?php the_sub_field('client_feedback'); ?></p>
						          		<p><i class="fa fa-star"></i> &nbsp <i class="fa fa-star"></i> &nbsp <i class="fa fa-star"></i> &nbsp <i class="fa fa-star"></i> &nbsp <i class="fa fa-star"></i></p>
						          		<p><b><?php the_sub_field('client_name'); ?></b></p>
						          	</center>
						        </div>
						        <?php
			    			
						    		endwhile;

						    	else:
						    		
						    	endif; 

						    	?>	          

					        </div>

					    </div>
					</div>
				</div>
			</div>
		</section>
		
		<?php /*
		<section class="home-testimonials" style="background-image: url('<?php echo $testibackimg['url']; ?>');">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="testi-icon">
		   					<div class="testi-inner-icon">
		   						
		   					</div>
		   					<i class="ti-eye"></i>
		   				</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						
						<center><php /* id="testimoni-firp" class=""><?php the_field('testimonials_tagline'); ?></p>
						<hr>
						<p id="testimoni-heading" class="font-50"><?php the_field('testimonials_title_1'); ?> <span><b><?php the_field('testimonials_title_2'); ?></b></span></p></center>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" id="patient-feedback">
						<div class="blackquote">
							<?php the_field('testimonials_content_1'); ?>
						</div>
						<p class="text-right testi-patient"><b><?php the_field('testimonials_customer_name_1'); ?></b></p>
					</div>
					<div class="col-md-4" id="patient-feedback">
						<div class="blackquote">
							<?php the_field('testimonials_content_2'); ?>
						</div>
						<p class="text-right testi-patient"><b><?php the_field('testimonials_customer_name_2'); ?></b></p>
					</div>
					<div class="col-md-4" id="patient-feedback">
						<div class="blackquote">
							<?php the_field('testimonials_content_3'); ?>
						</div>
						<p class="text-right testi-patient"><b><?php the_field('testimonials_customer_name_3'); ?></b></p>
					</div>
				</div>
			</div>
		</section> */ ?>

		<section class="getintouch padding-50-20 blue-grey-background">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p class="font-25"><?php the_field('get_in_touch_title_1'); ?> <span class="skyblue"><b><?php the_field('get_in_touch_title_2'); ?></b></span></p>
						<hr>
						<p class=""><?php the_field('get_in_touch_content'); ?></p>
					</div>
					<div class="col-md-6 getintouch-info">
						<div class="row">
							<div class="col-sm-4 text-center">
								<!-- <div class="getin-icon icon-animate">
				   					<div class="getin-inner-icon">
				   						
				   					</div>
				   					<i class="fa fa-building-o"></i>
				   				</div> -->
								<!-- <i class="fa fa-building-o"></i> -->
								<div class="getin-cont">
									<i class="fa fa-building-o getin-hover"></i>
									<center><hr></center>
									<p class="getin-hover"><b><?php the_field('get_in_touch_address_title'); ?></b></p>
									<p><?php the_field('get_in_touch_address'); ?></p>
								</div>
								
							</div>
							<div class="col-sm-4 text-center">
								<!-- <div class="getin-icon icon-animate">
				   					<div class="getin-inner-icon">
				   						
				   					</div>
				   					<i class="fa fa-mobile"></i>
				   				</div> -->
								<!-- <i class="fa fa-mobile"></i> -->
								<div class="getin-cont">
									<i class="fa fa-mobile getin-hover"></i>
									<center><hr></center>
									<p class="getin-hover"><b><?php the_field('get_in_touch_phone_text'); ?></b></p>
									<p><?php the_field('get_in_touch_phone'); ?></p>
								</div>
								
							</div>
							<div class="col-sm-4 text-center">
								<!-- <div class="getin-icon icon-animate">
				   					<div class="getin-inner-icon">
				   						
				   					</div>
				   					<i class="fa fa-envelope-o"></i>
				   				</div> -->
								<!-- <i class="fa fa-file-text-o"></i> -->
								<div class="getin-cont">
									<i class="fa fa-envelope-o getin-hover"></i>
									<center><hr></center>
									<p class="getin-hover"><b><?php the_field('get_in_touch_mail_text'); ?></b></p>
									<p><?php the_field('get_in_touch_mail'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php the_field('map'); ?>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

<script>
	    $(document).ready(function() {
	      $("#owl-demo").owlCarousel({
	        navigation : true
	      });
	    });

    </script>