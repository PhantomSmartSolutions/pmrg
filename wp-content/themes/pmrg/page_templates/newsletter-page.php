<?php
/**
 * Template Name: Newsletter Page
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>

<style type="text/css">
	a:hover,a:focus{
    text-decoration: none;
    outline: none;
}
#accordion .panel{
    border: none;
    border-radius: 0;
    box-shadow: none;
    margin-bottom: 15px;
    position: relative;
}
#accordion .panel:before{
    content: "";
    display: block;
    width: 1px;
    height: 100%;
    border: 1px dashed #6e8898;
    top: 25px;
    left: 18px;
    position: absolute;
}
#accordion .panel:last-child:before{ display: none; }
#accordion .panel-heading{
    padding: 0;
    border: none;
    border-radius: 0;
    position: relative;
}
#accordion .panel-title a{
    display: block;
    padding: 10px 30px 10px 60px;
    margin: 0;
    background: #fff;
    font-size: 18px;
    font-weight: 700;
    color: #1d3557;
    letter-spacing: 1px;
    border-radius: 0;
    overflow: hidden;
    position: relative;
}
#accordion .panel-title a:before,
#accordion .panel-title a.collapsed:before{
    content: "\f107";
    font-family: "FontAwesome";
    font-weight: 900;
    width: 40px;
    height: 100%;
    line-height: 40px;
    font-size: 17px;
    color: #fff;
    text-align: center;
    background: #00b3cc;
    border-radius: 3px;
    border: 1px solid #00b3cc;
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.3s ease 0s;
}
#accordion .panel-title a.collapsed:before{
    content: "\f105";
    color: #000;
    background: #fff;
    border: 1px solid #6e8898;
}
#accordion .panel-body{
    margin-left: 40px;
    border-top: none;
    line-height: 28px;
    letter-spacing: 1px;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: white;
    background-color: #30b2d2;
    border-left: 2px solid #30b2d2;
    border-radius: 0px;
    font-weight: bold;
    border-bottom: 1px solid #30b2d2;
    border-right: 2px solid #30b2d2;
    border-top: 1px solid #30b2d2;
}
.nav-pills>li>a {
    border-radius: 0px;
    color: #30b2d2;
    border-right: 1px solid #e1e1e1;
    font-weight: bold;
    border-bottom: 1px solid #e1e1e1;
    border-left: 1px solid #e1e1e1;
}
.nav-stacked>li+li {
    margin-top: 0px;
    margin-left: 0;
}
.nav-stacked>li{ padding-left: 0px !important; padding-top: 0px !important; }
	.newsletter-gallery img{ margin-bottom:30px; }
@media only screen and (max-width: 480px){
    #accordion .panel-body{ padding: 70px 10px; }
}
</style>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	$bannerimg = get_field('banner_image_1');
?>

    	<section class="background-img padding-150-30" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
			<div class="container">
				<div class="row">
		   			<!-- <div class="col-md-2"></div> -->
		   			<div class="col-md-12 text-center">
		   				<p class="font-20"><?php the_field('banner_title'); ?></p>
		   				<center><hr class="hr-center"></center>
		   				<p class="font-25"><span><b><?php the_title(); ?></b></span></p>
		   				<!-- <p class="font-16"><i>I endorse PMRG enthusiastically. Without this company, we could not have achieved the financial success, patient satisfaction and reduced stress for the partners so quickly and effectively.</i><br>– Adam Friend, MD, Managing Partner, Eye Care Associates of New Jersey</p> -->
		   			</div>
		   			<!-- <div class="col-md-2"></div> -->
		   		</div><br>
		   		
			</div>
		</section>
		
		<!-- <section class="newsletter-banner pos-relative">
			
		</section> -->

		<section class="policy-content list-before newsletter-border">
			<!-- <div class="newsletter-border-group">
				<div class="newsletter-border-skblue"></div>
				<div class="newsletter-border-lgreen"></div>
				<div class="newsletter-border-trans"></div>
				<div class="newsletter-border-dblue"></div>
			</div> -->
			<div class="newsletter-border-group-2">
				<div class="newsletter-border-skblue-2"></div>
				<div class="newsletter-border-lgreen-2"></div>
				<!-- <div class="newsletter-border-trans-2"></div>
				<div class="newsletter-border-dblue-2"></div> -->
			</div>


			<div class="container">

				<div class="row margin-75">
					<div class="col-sm-1">
						
					</div>
					<div class="col-sm-10">
						<?php $bannerimg2 = get_field('banner_image_2') ?>
						<img src="<?php echo $bannerimg2['url']; ?>" alt="<?php echo $bannerimg2['alt']; ?>">
					</div>
					<div class="col-sm-1">
						
					</div>
				</div>

				<div class="row"><br><br><br>
					<div class="col-md-12 olanimate hidden">
						<?php the_field('newsletter_content_1'); ?>
					</div>
				</div><br><br>

			    <div class="row newsletter-border-right">
			    	<div class="col-md-1">
			    		
			    	</div>
			        <div class="col-md-11">
			            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingOne">
			                        <h4 class="panel-title">
			                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			                                <?php the_field('accordion_section_title_1'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			                        <div class="panel-body">
			                            <?php the_field('accordion_section_content_1'); ?>
			                        </div>
			                    </div>
			                </div>
			                <?php 
					      	$i = 1;
					      	if( have_rows('accordion_sections') ):

						 	// loop through the rows of data
						    while ( have_rows('accordion_sections') ) : the_row();
					      ?>
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="heading-a<?php echo $i; ?>">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-a<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-a<?php echo $i; ?>">
			                                <?php the_sub_field('accordion_title'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapse-a<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-a<?php echo $i; ?>">
			                        <div class="panel-body">
			                            <?php the_sub_field('accordion_content'); ?>
			                        </div>
			                    </div>
			                </div>
			                <?php
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>

			                <!-- <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingTwo">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                                <?php the_field('accordion_section_title_2'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			                        <div class="panel-body">
			                            <?php the_field('accordion_section_content_2'); ?>
			                        </div>
			                    </div>
			                </div>
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingThree">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			                                <?php the_field('accordion_section_title_3'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			                        <div class="panel-body">
			                            <?php the_field('accordion_section_content_3'); ?>
			                        </div>
			                    </div>
			                </div>
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingFour">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			                                <?php the_field('accordion_section_title_4'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
			                        <div class="panel-body">
			                            <?php the_field('accordion_section_content_4'); ?>
			                        </div>
			                    </div>
			                </div>
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingFive">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			                                <?php the_field('accordion_section_title_5'); ?>
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
			                        <div class="panel-body">
			                            <?php the_field('accordion_section_content_5'); ?>
			                        </div>
			                    </div>
			                </div> -->
			            </div>
			        </div>
			        <!-- <div class="col-md-1 ">
			    		
			    	</div> -->
			    </div><br><br>
			
				<div class="row">
					<div class="col-md-12">
						<?php the_field('newsletter_content_2'); ?>
					</div>
				</div><br><br>
				<div class="row newsletter-border-right">
					<div class="col-sm-4 col-md-3 no-padding">
						<ul class="nav nav-pills nav-stacked" id="newsletter-tab-ul">
						    <li class="active"><a data-toggle="pill" href="#news-tab1"><?php the_field('tab_section_title_1'); ?></a></li>
						    <?php 
					      	$i = 2;
					      	if( have_rows('tab_sections_titles') ):

						 	// loop through the rows of data
						    while ( have_rows('tab_sections_titles') ) : the_row();
					      ?>
						    <li><a data-toggle="pill" href="#news-tab<?php echo $i; ?>"><?php the_sub_field('tab_section_title'); ?></a></li>
						    <?php
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>
						    <!-- <li><a data-toggle="pill" href="#news-tab3"><?php the_field('tab_section_title_3'); ?></a></li>
						    <li><a data-toggle="pill" href="#news-tab4"><?php the_field('tab_section_title_4'); ?></a></li>
						    <li><a data-toggle="pill" href="#news-tab5"><?php the_field('tab_section_title_5'); ?></a></li> -->
						</ul>
					</div>
					<div class="col-sm-8 col-md-9">
						<div class="tab-content newsletter-tab-sec">
						    <div id="news-tab1" class="tab-pane fade in active">
							      <?php the_field('tab_section_content_1'); ?>
						    </div>
						    <?php 
					      	$i = 2;
					      	if( have_rows('tab_section_contents') ):

						 	// loop through the rows of data
						    while ( have_rows('tab_section_contents') ) : the_row();
					      ?>
						    <div id="news-tab<?php echo $i; ?>" class="tab-pane fade">
							    <?php the_sub_field('tab_section_content'); ?>
						    </div>
						    <?php
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>

						    <!-- <div id="news-tab3" class="tab-pane fade">
							    <?php the_field('tab_section_content_3'); ?>
						    </div>
						    <div id="news-tab4" class="tab-pane fade list-before">
							    <?php the_field('tab_section_content_4'); ?>
						    </div>
						    <div id="news-tab5" class="tab-pane fade list-before">
							    <?php the_field('tab_section_content_5'); ?>
						    </div> -->
						</div>
					</div>
				</div><br><br>
						

				<div class="row">
					<div class="col-md-12 olanimate hidden">
						<?php the_field('newsletter_content_3'); ?>
						
					</div>
				</div><br><br>

				<div class="row">
					<center><p class="font-20 text-center" style="text-align: center !important;"><?php the_field('gallery_title'); ?></p>
		   				<hr class="hr-center"></center>
					<?php 
					      	if( have_rows('gallery') ):

						 	// loop through the rows of data
						    while ( have_rows('gallery') ) : the_row();
						    	// if( get_row_index() % 2 == 0 ){
						    	$galimg = get_sub_field('image');
					      ?>
					<div class="col-sm-4 newsletter-gallery">
						<img src="<?php echo $galimg['url'] ?>" alt="<?php echo $galimg['alt'] ?>">
					</div>
					<?php
					        endwhile;

							else :

							    // no rows found

							endif;

							?>
				</div><br><br><br>
			</div>
		</section>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>