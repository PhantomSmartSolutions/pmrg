<?php
/**
 * Template Name: Who We Are
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/theme.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/timeline.scss">
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/theme.min.js"></script>

<style>
	#menu-main-menu #menu-item-383 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	header.fixed #menu-main-menu #menu-item-383 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php
		$bannerimg = get_field('banner_image');
	?>

    <section class="abt-banner padding-150-30 background-img" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
			<div class="container">
				<div class="row">
					<!-- <div class="col-md-6">
						
					</div> -->
					<div class="col-md-12 text-center">
						<center>
							<?php /* <p class="font-20"><?php the_field('banner_tagline'); ?></p> */ ?>
							<p class="font-20"><?php the_field('banner_title'); ?></p>
							<hr>
							<?php /*<p class="banner-h1 olanimate hidden"><?php the_field('banner_title_1'); ?> <span class="skyblue"><b><?php the_field('banner_title_2'); ?></b></span></p> */ ?>
							<p class="font-14"><?php the_field('banner_content'); ?></p>
						</center>
					</div>
				</div>
			</div>
		</section>

		<section class="abt-fir-sec padding-50-0">
			<div class="container">
				<div class="row">
		   			<!-- <div class="col-md-2"></div> -->
		   			<div class="col-md-12 text-center">
		   				<p class="font-17 color-21"><?php the_field('who_we_are_tagline'); ?></p>
		   				<center><hr class="hr-center"></center>
		   				<p class="font-25 color-21"><?php the_field('who_we_are_title_1'); ?> <span class="skyblue"><b><?php the_field('who_we_are_title_2'); ?></b></span></p>
		   				<?php /*<p class="font-16"><i><?php the_field('who_we_are_quotes'); ?></i><br><?php the_field('who_we_are_quotes_author'); ?></p> */ ?>
		   			</div>
		   			<!-- <div class="col-md-2"></div> -->
		   		</div><br>
		   	</div>
		   	<?php
			$section1img = get_field('who_we_are_section_1_image');
			$section2img = get_field('who_we_are_section_2_image');
			$section3img = get_field('who_we_are_section_3_image');
			$section4img = get_field('who_we_are_section_4_image');
			?>
		   	<div class="row no-margin">
		   		<div class="col-sm-6 col-lg-3 whoweare-thumbnail no-padding">
		   			<div class="img-overflow">
		   				<img src="<?php echo $section1img['url']; ?>" alt="<?php echo $section1img['alt']; ?>">
		   			</div>
		   			
		   			<div class="caption">
		   				<span class="top-left-angle"></span>
		   				<i class="fa fa-users"></i>
		   				<p class="font-17 skyblue"><b><?php the_field('who_we_are__section_2_title'); ?></b></p>
		   				<hr class="hr-center">
		   				<p class=""><?php // the_field('who_we_are_section_2_content'); 
		   					$content1 = get_field('who_we_are_section_2_content'); 
							echo wp_trim_words( $content1 , '30' );  
							?></p>
						<p class="skyblue" id="mgmt-href" style="float:right;font-weight:bold;">Read More</p>
		   			</div>
		   		</div>
		   		<div class="col-sm-6 col-lg-3 whoweare-thumbnail no-padding">
		   			<div class="img-overflow">
		   				<img src="<?php echo $section2img['url']; ?>" alt="<?php echo $section2img['alt']; ?>">
		   			</div>
		   			
		   			<div class="caption">
		   				<span class="top-left-angle"></span>
		   				<i class="fa fa-eye"></i>
		   				<p class="font-17 skyblue"><b><?php the_field('who_we_are__section_1_title'); ?></b></p>
		   				<hr class="hr-center">
		   				<p class=""><?php //the_field('who_we_are_section_1_content'); 
		   					$content2 = get_field('who_we_are_section_1_content'); 
							echo wp_trim_words( $content2 , '30' );  
		   				?></p>
						<p class="skyblue" id="perspective-href" style="float:right;font-weight:bold;">Read More</p>
		   			</div>
		   		</div>
		   		
		   		<div class="col-sm-6 col-lg-3 whoweare-thumbnail no-padding">
		   			<div class="img-overflow">
		   				<img src="<?php echo $section3img['url']; ?>" alt="<?php echo $section3img['alt']; ?>">
		   			</div>
		   			
		   			<div class="caption">
		   				<span class="top-left-angle"></span>
		   				<i class="fa fa-history"></i>
		   				<p class="font-17 skyblue"><b><?php the_field('who_we_are_section_3_title'); ?></b></p>
		   				<hr class="hr-center">
		   				<p class=""><?php // the_field('who_we_are_section_3_content'); 
		   					$content3 = get_field('who_we_are_section_3_content'); 
							echo wp_trim_words( $content3 , '30' );  
		   				?></p>
						<p class="skyblue" id="history-href" style="float:right;font-weight:bold;">Read More</p>
		   			</div>
		   		</div>
		   		<div class="col-sm-6 col-lg-3 whoweare-thumbnail no-padding">
		   			<div class="img-overflow">
		   				<img src="<?php echo $section4img['url']; ?>" alt="<?php echo $section4img['alt']; ?>">
		   			</div>
		   			
		   			<div class="caption">
		   				<span class="top-left-angle"></span>
		   				<i class="fa fa-user-md"></i>
		   				<p class="font-17 skyblue"><b><?php the_field('who_we_are_section_4_title'); ?></b></p>
		   				<hr class="hr-center">
		   				<p class=""><?php //the_field('who_we_are_section_4_content'); 
		   					$content4 = get_field('who_we_are_section_4_content'); 
							echo wp_trim_words( $content4 , '30' );  
		   				?></p>
						<p class="skyblue" id="staff-href" style="float:right;font-weight:bold;">Read More</p>
		   			</div>
		   		</div>
		   	</div>
		   	
		</section>

		<section class="our-team padding-50-0 blue-grey-background" id="mgmt-team"><br>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center><p class="font-17"><?php the_field('management_team_tagline'); ?></p>
						<hr>
						<p class="font-25 margin-10"><?php the_field('management_team_title_1'); ?>	<span class="skyblue"><b><?php the_field('management_team_title_2'); ?></b></span></p></center>
					</div>
				</div>
				<?php 
					$memimg1 = get_field('team_member_image_1');
					$memimg2 = get_field('team_member_image_2');
					$memimg3 = get_field('team_member_image_3');
					$memimg4 = get_field('team_member_image_4');
					$memimg5 = get_field('team_member_image_5');
					$memimg6 = get_field('team_member_image_6');
					$memimg7 = get_field('team_member_image_7');
					$memimg8 = get_field('team_member_image_8');
				?><br>
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg1['url']; ?>" alt="<?php echo $memimg1['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_1'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_1'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_1'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_1'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg2['url']; ?>" alt="<?php echo $memimg2['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_2'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_2'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_2'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_2'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
				<div class="row">
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg3['url']; ?>" alt="<?php echo $memimg3['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_3'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_3'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_3'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_3'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg4['url']; ?>" alt="<?php echo $memimg4['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_4'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_4'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_4'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_4'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg5['url']; ?>" alt="<?php echo $memimg5['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_5'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_5'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_5'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_5'); ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg6['url']; ?>" alt="<?php echo $memimg6['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_6'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_6'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_6'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_6'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg7['url']; ?>" alt="<?php echo $memimg7['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_7'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_7'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_7'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_7'); ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 leader-gap">
						<div class="leadership-member">
							<div class="leadership-image">
								<img src="<?php echo $memimg8['url']; ?>" alt="<?php echo $memimg8['alt']; ?>" style="width: 100%;">
								<div class="leadership-content">
									<div class="leadership-innerdiv">
										<p><?php echo the_field('team_member_content_8'); ?></p>
										<a href="<?php echo the_field('team_member_button_link_8'); ?>" class="btn"><?php echo the_field('team_member_read_more_button'); ?></a>
									</div>
								</div>
							</div>
							<div class="leadership-info">
								<p class="mgmt-member-name"><?php echo the_field('team_member_name_8'); ?></p>
								<p class="mgmt-member-role"><?php echo the_field('team_member_designation_8'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		


		<?php
			$ourstaffimg = get_field('our_staff_background_image');
		?>

		<section class="abt-staff justify-text padding-50-20" id="our-staff"><br><br>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p class="font-25 skyblue"><b><?php the_field('our_staff_title'); ?></b></p>
						<hr class="hr-center">
						<?php the_field('our_staff_content_1'); ?>
						<?php the_field('our_staff_content_2'); ?>
					</div>
					<div class="col-md-6">
						<img src="<?php echo $ourstaffimg['url']; ?>" alt="<?php echo $ourstaffimg['alt']; ?>">
					</div>
				</div>
			</div><br>
		</section>
		<section class="justify-text padding-50-20" id="our-perspective"><br><br>
		<?php
			$perspectiveimg = get_field('our_perspective_background_image');
		?>
			
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img src="<?php echo $perspectiveimg['url']; ?>" alt="<?php echo $perspectiveimg['alt']; ?>">
					</div>
					<div class="col-md-6">
						<p class="font-25 margin-10 skyblue"><b><?php the_field('our_perspective_title'); ?></b></p>
						<hr class="hr-center">
						<?php the_field('our_perspective_content_1'); ?>
						<?php the_field('our_perspective_content_2'); ?>
					</div>
				</div>
			</div><br>
		</section>

		<div class="timeline-page padding-50-20" id="our-history">
				<div class="row">
					<div class="col-md-12">
						<center>
						<p class="font-25 skyblue"><b><?php the_field('our_history_title'); ?></b></p></center>
					</div>
				</div><br>


			            	<div class="timeline-wrapper" style="margin-top: 0px;">
							    <div class="container">
							    <?php
			                     $i = 1;
			                        if ( have_rows('history_timeline') ):
			                        while ( have_rows('history_timeline') ) : the_row();
			                        	$historyimg = get_sub_field('history_image');
			                           if( !($i % 2)==0 ) {
			                     ?>
							      <div class="year">
							        <span><?php the_sub_field('history_year'); ?></span>
							      </div>
							      <div class="node left">
							        <div class="marker"></div>
							        <div class="entry" style="">
							        	<img src="<?php echo $historyimg['url'] ?>" alt="<?php echo $historyimg['alt'] ?>">
							        </div>
							      </div>
							      <div class="node">
							        <div class="marker"></div>
							        <div class="entry short-entry">
							          <?php the_sub_field('history_content'); ?>
							        </div>
							      </div>
							      <div class="clearfix"></div>
							      <?php
				                   }
				                         else {
				                        ?>
							      <div class="year">
							        <span><?php the_sub_field('history_year'); ?></span>
							      </div>
							      <div class="node">
							        <div class="marker"></div>
							        <div class="entry" style="">
							        	<img src="<?php echo $historyimg['url'] ?>" alt="<?php echo $historyimg['alt'] ?>">
							        </div>
							      </div>
							      <div class="node left">
							        <div class="marker"></div>
							        <div class="news">
							          <section>
							            <?php the_sub_field('history_content'); ?>
							          </section>
							        </div>
							      </div>
							      <div class="clearfix"></div>
							      <?php
				                   }
				                         $i++;
				                           endwhile;
				                        else:
				                        endif;
				                        ?>
							      
							      <div class="year last">
							        <span>&infin;</span>
							      </div>
							    </div>
							</div>
					</div>


		<?php /*
		<section class="padding-100-20" id="our-history">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center>
						<p class="font-50 margin-25 skyblue"><b><?php the_field('our_history_title'); ?></b></center>
					</div>
				</div><br>
				
				<div class="desktop-history">
					<div id="conference-timeline">
					    <div class="timeline-start"><?php the_field('history_start'); ?></div>
					    <div class="conference-center-line"></div>
					    <div class="conference-timeline-content">
					      <!-- Article -->
					      <?php 
					      	$i = 1;
					      	if( have_rows('history_content') ):

						 	// loop through the rows of data
						    while ( have_rows('history_content') ) : the_row();
						    	// if( get_row_index() % 2 == 0 ){
						    	$historyimg = get_sub_field('history_image');
					      ?>
					      <?php if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 9 || $i == 11 || $i == 13 || $i == 15 ) { ?>
					      	
						        
					      	<div class="timeline-article">
								<div class="content-left-container">
						          <div class="content-left">
						            <img src="<?php echo $historyimg['url']; ?>" alt="<?php echo $historyimg['alt']; ?>">
						          </div>
						        </div>
						        <div class="content-right-container">
						          <div class="content-right">
						            <?php the_sub_field('history_content'); ?>
						          </div>
						        </div>
						        <div class="meta-date">

						        </div>
						      </div>
						      

					      	<?php
					      	
					      	
					      	// } else{
					       }  if( $i == 2 || $i == 4 || $i == 6 || $i == 8 || $i == 10 || $i == 12 || $i == 14 || $i == 16 ) {
					      	?>
					      
					      	
						      <div class="timeline-article">
						        <div class="content-left-container">
						          <div class="content-left">
						            <?php the_sub_field('history_content'); ?>
						          </div>
						        </div>
						        <div class="content-right-container">
						          <div class="content-right">
						            <img src="<?php echo $historyimg['url']; ?>" alt="<?php echo $historyimg['alt']; ?>">
						          </div>
						        </div>
						        <div class="meta-date">
						        </div>
						      </div>

					      <?php
					      }
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>
					    </div>
					  </div>
				</div>
			</div>
		</section> */ ?>

		

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

<script>
	$(document).ready(function() {
		$("#staff-href").click(function() {
			$("html, body").animate({
				scrollTop: $("#our-staff").offset().top
			}, 400);
		});

		$("#history-href").click(function() {
			$("html, body").animate({
				scrollTop: $("#our-history").offset().top
			}, 1000);
		});

		$("#perspective-href").click(function() {
			$("html, body").animate({
				scrollTop: $("#our-perspective").offset().top
			}, 600);
		});

		$("#mgmt-href").click(function() {
			$("html, body").animate({
				scrollTop: $("#mgmt-team").offset().top
			}, 200);
		});
	});
</script>