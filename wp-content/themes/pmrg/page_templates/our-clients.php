<?php
/**
 * Template Name: Our Clients
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>

<style type="text/css">
	.slick-slide {
	    margin: 0px 20px;
	}
	#menu-main-menu #menu-item-375 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	header.fixed #menu-main-menu #menu-item-375 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	$bannerimg = get_field('banner_image');
?>

    <section class="padding-150-30 background-img" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
		<div class="container">
			<div class="row">
	   			<!-- <div class="col-md-2"></div> -->
	   			<div class="col-md-12 text-center">
	   				<p class="font-20"><?php the_field('banner_title'); ?></p>
	   				<center><hr class="hr-center"></center>
	   				
	   				<p class="font-14"><?php the_field('banner_content'); ?></p>
					
	   			</div>
	   			<!-- <div class="col-md-2"></div> -->
	   		</div><br>
	   		
		</div>
	</section>

	<section class="padding-50-20 client-fir-sec list-before">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<center><p class="font-25"><b><?php the_field('our_clients_title_1'); ?> <span class="skyblue"><?php the_field('our_clients_title_2'); ?></span></b></p>
					<hr class="hr-center"></center>
					<?php the_field('our_clients_content'); ?>
				</div>
			</div>
		</div>
	</section>

	

	<?php /*
	<section class="padding-50-20" >
	   <div class="container">
		    <h2><?php the_field('client_logo_title'); ?></h2>
		    <div class="customer-logos slider">
		    	<?php
		    		if ( have_rows('client_logos') ):
		    			while ( have_rows('client_logos') ) : the_row();
		    				$clientlogo = get_sub_field('client_logo');
		    				?>

		    				<div class="slide"><img src="<?php echo $clientlogo['url']; ?>" alt="<?php echo $clientlogo['alt']; ?>"></div>
		    				<?php
		    			
		    		endwhile;

		    	else:
		    		
		    	endif;

		    	?>
		    </div>
		</div>
	</section> */ ?>

	

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
<script>
	    $(document).ready(function() {
	      $("#owl-demo").owlCarousel({
	        navigation : true
	      });
	    });

    </script>