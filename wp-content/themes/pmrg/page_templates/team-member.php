<?php
/**
 * Template Name: Team Member
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	$bannerimg = get_field('banner_image');
?>

    <section class="serve-banner padding-150-30 background-img" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12 text-center">
					<center>
						<p class="font-20"><?php the_field('banner_title'); ?></p>
						<hr>
						<?php /* <p class="banner-h1 olanimate hidden"><?php the_field('banner_title_1'); ?> <span class="skyblue"><b><?php the_field('banner_title_2'); ?></b></span></p> */ ?>
						<p class="font-14"><?php the_field('banner_content'); ?></p>
					</center>
				</div>
			</div>
		</div>
	</section>

<?php
	$img = get_field('image');
?>
	<section class="padding-50-20">
		<div class="container">
			<div class="row">
				<div class="col-sm-4" style="margin-bottom: 30px;">
					<img src="<?php echo $img['url']; ?>" style="width:100%;">
				</div>
				<div class="col-sm-8 text-justify">
					<?php the_field('about_the_member'); ?>
				</div>
			</div>
		</div>
	</section>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>