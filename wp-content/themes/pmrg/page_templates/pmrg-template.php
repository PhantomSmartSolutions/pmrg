<?php
/**
 * Template Name: PMRG Template
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php
	$bannerimg = get_field('banner_image');
?>

    	<section class="background-img padding-150-30" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
			<div class="container">
				<div class="row">
		   			<!-- <div class="col-md-2"></div> -->
		   			<div class="col-md-12 text-center">
		   				<!-- <p class="font-20 olanimate hidden">Newsletter</p> -->
		   				
		   				<p class="font-25"><span><b><?php the_title(); ?></b></span></p>
		   				<center><hr class="hr-center"></center>
						<p><?php the_field('banner_content'); ?></p>
		   				<!-- <p class="font-16"><i>I endorse PMRG enthusiastically. Without this company, we could not have achieved the financial success, patient satisfaction and reduced stress for the partners so quickly and effectively.</i><br>– Adam Friend, MD, Managing Partner, Eye Care Associates of New Jersey</p> -->
		   			</div>
		   			<!-- <div class="col-md-2"></div> -->
		   		</div><br>
		   		
			</div>
		</section>

		<section class="policy-content padding-50-20">
			<div class="container">
				<div class="row">
					<div class="col-md-12 list-before">
						<?php the_field('pmrg_content'); ?>
						
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>