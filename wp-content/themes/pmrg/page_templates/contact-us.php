<?php
/**
 * Template Name: Contact Us Page
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>

<style>
	#menu-main-menu #menu-item-374 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	header.fixed #menu-main-menu #menu-item-374 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	$bannerimg = get_field('banner_image');
?>

    <section class="padding-150-30 background-img" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
		<div class="container">
			<div class="row">
	   			<!-- <div class="col-md-2"></div> -->
	   			<div class="col-md-12 text-center">
	   				<p class="font-20"><?php the_field('banner_title'); ?></p>
	   				<center><hr class="hr-center"></center>
					<p class="font-14"><b><?php the_field('main_content'); ?></b></p>
	   			</div>
	   			<!-- <div class="col-md-2"></div> -->
	   		</div><br>
	   		
		</div>
	</section>


	<!-- <section class="contact-form padding-100-20 blue-grey-background">
	    <div class="container">
	   		
	   	</div>
	</section> -->

	<section class="" style=""><br><br>
    <div class="container">
    	<div class="row">
	   			<div class="col-md-12 olanimate hidden">
	   				<center><p class="font-20"><b><?php the_field('contact_us_title_1'); ?> <span class="skyblue"><?php the_field('contact_us_title_2'); ?></span></b></p>
	   	   			<hr class="hr-center">
	   	   			<p class="font-14"><?php the_field('content'); ?></p></center>
	   			</div>
	   			<!-- <div class="col-md-5"></div> -->
	   		</div><br>
        <div class="row">
            <div class="col-md-12">

                <center><ul class="nav nav-tabs border-bottom-0 justify-content-center m-tabs" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#mtab1" role="tab" aria-controls="home" aria-selected="true">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#mtab2" role="tab" aria-controls="profile" aria-selected="false">Map</a>
                    </li>
                </ul></center>
                <div class="tab-content">
                    <div class="tab-pane active" id="mtab1" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row shadow-4 rounded bg-white justify-content-center">
                            <div class="col-sm-12 col-md-5 bg-primary p-0 text-white">
                            	<?php $cpbackimg = get_field('mail_image'); ?>
                                <div class="card border-0 rounded bg-img p-3 py-4 h-100" style="background-image: url('<?php echo $cpbackimg['url']; ?>');padding: 20px 40px !important;" data-overlay="4">
                                    <div class="row h-100 p-5">
                                        <div class="col-12">
                                            <h4><?php the_field('contact_image_title'); ?></h4>
                                            <p class="cp-address"><?php the_field('executive_office_address'); ?></p>
                                            <p class="cp-mail"><?php the_field('other_inquiries_mail'); ?></p>
                                            <p class="cp-mobile"><?php the_field('general_inquiries_phone'); ?></p>
                                        </div><br><br><br><br><br><br><br><br><br><br><br>
                                        <!--div class="col-12 align-self-end">
                                            <div>
                                                <h6>Follow Us</h6>
                                                <div class="social social-bg-dark">
                                                    <a class="social-twitter cp" href="#"><i class="fa fa-twitter"></i></a>&nbsp &nbsp &nbsp
                                                    <a class="social-facebook cp" href="#"><i class="fa fa-facebook"></i></a> &nbsp &nbsp &nbsp
                                                    <a class="social-instagram cp" href="#"><i class="fa fa-instagram"></i></a> &nbsp &nbsp &nbsp
                                                    
                                                </div>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7 p-8 py-7">
                                <?php the_field('contact_form_shortcode'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="mtab2" role="tabpanel" aria-labelledby="profile-tab">
                        <?php the_field('contact_map'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div><br>
</section>

	<section class="blue-grey-background padding-50-20">
		<div class="container">
	   		<div class="row">
	   			<div class="col-md-12">
					<center><p class="font-20"><b><?php the_field('contact_pmrg_title_1'); ?> <span class="skyblue"><?php the_field('contact_pmrg_title_2'); ?></span></b></p>
			   		<hr class="hr-center"></center>
	   			</div>
	   		</div>
	   	    <div class="row">
	   	   		<div class="col-sm-4 col-md-4 col-lg-4 olanimate hidden">
	   	   			<div class="">
						<p class="skyblue font-18"><b><?php the_field('general_inquiries_title'); ?></b></p>
						<p><?php the_field('general_inquiries_phone'); ?></p>
					</div>
					<div class="">
						<p class="skyblue font-18"><b><?php the_field('sales_and_marketing_title'); ?></b></p>
						<p><?php the_field('sales_and_marketing_phone'); ?></p>
						<p><?php the_field('sales_and_marketing_mail'); ?></p>
					</div>
					<div class="">
						<?php /* <p class="skyblue font-18"><b><?php the_field('executive_office_title'); ?></b></p>
						<p><?php the_field('executive_office_address'); ?></p> */ ?>
					</div>
					<div class="">
						<p class="skyblue font-18"><b><?php the_field('operations_center_title'); ?></b></p>
						<p><?php the_field('operations_center_address'); ?></p>
						<?php /* <p><?php the_field('operations_center_phone'); ?></p>
						<p><?php the_field('operations_center_mail'); ?></p>*/ ?>
					</div>
	   	   		</div>
	   	   		<div class="col-sm-4 col-md-4 col-lg-4 olanimate hidden">
	   	   			<div class="img-overlay">
	   	   				<?php $building = get_field('builing_image'); ?>
						<figure><img src="<?php echo $building['url']; ?>" alt="<?php echo $building['alt']; ?>"></figure>
					</div>
	   	   		</div>
	   	   		<div class="col-sm-4 col-md-4 col-lg-4 olanimate hidden contact-alright">
					
					
					<div class="">
						<p class="skyblue font-18"><b><?php the_field('other_inquiries_title'); ?></b></p>
						<p><?php the_field('other_inquiries_mail'); ?></p>
					</div>
					<div class="">
						<p class="skyblue font-18"><b><?php the_field('help_desk_title'); ?></b></p>
						<p><?php the_field('help_desk_phone'); ?></p>
						<p><?php the_field('help_desk_mail'); ?></p>
					</div><br>
					<div class="img-overlay">
	   	   				<?php $downloadimg = get_field('file_download_image'); ?>
						<figure><a href="<?php the_field('file_download_link'); ?>"><img src="<?php echo $downloadimg['url']; ?>" alt="<?php echo $downloadimg['alt']; ?>"></a></figure>
					</div>
	   	   		</div>
	   	   		
	   	    </div>
	    </div>
	</section>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>