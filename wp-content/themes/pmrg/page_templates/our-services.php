<?php
/**
 * Template Name: Our Services
 *
 * @package WordPress
 * @subpackage project name
 */

get_header(); ?>

<style>
	#menu-main-menu #menu-item-376 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	header.fixed #menu-main-menu #menu-item-376 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }+
	.other-serve a{ color: #30b2d2 !important; }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php
		$bannerimg = get_field('banner_image');
		$serveimg1 = get_field('our_services_section_1_image');
		$serveimg2 = get_field('our_services_section_2_image');
		$serveimg3 = get_field('our_services_section_3_image');
		$serveimg4 = get_field('our_services_section_4_image');
		$billimg1 = get_field('billing_service_tab_image_1');
		$billimg2 = get_field('billing_service_tab_image_2');
		$billimg3 = get_field('billing_service_tab_image_3');
		$billimg4 = get_field('billing_service_tab_image_4');
	?>

    <section class="serve-banner padding-150-30 background-img" style="background-image:url('<?php echo $bannerimg['url']; ?>');">
			<div class="container">
				<div class="row">
					
					<div class="col-md-12 text-center">
						<center>
							<p class="font-20"><?php the_field('banner_title'); ?></p>
							<hr>
							<?php /* <p class="banner-h1 olanimate hidden"><?php the_field('banner_title_1'); ?> <span class="skyblue"><b><?php the_field('banner_title_2'); ?></b></span></p> */ ?>
							<p class="font-14"><?php the_field('banner_content'); ?></p>
						</center>
					</div>
				</div>
			</div>
		</section>

		<section class="serve-fir-sec padding-50-20">
			<div class="container">
				<div class="row">
		   			<!-- <div class="col-md-2"></div> -->
		   			<div class="col-md-12 text-center">
		   				<?php /* <p class="font-17"><?php the_field('our_service_tagline'); ?></p> */ ?>
						<p class="font-17"><?php the_field('our_service_tagline'); ?></p>
		   				<center><hr class="hr-center"></center>
		   				<p class="font-25"><?php the_field('our_services_title_1'); ?> <span class="skyblue"><b><?php the_field('our_services_title_2'); ?></b></span></p>
		   				<?php /* <p class="font-16"><i><?php the_field('our_services_quotes'); ?></i><br><?php the_field('our_services_quotes_author'); ?></p> */ ?>
		   			</div>
		   			<!-- <div class="col-md-2"></div> -->
		   		</div><br>
		   		
		   		<div class="row">
		   			<div class="col-md-12">
		   				<div class="row">
		   					<div class="col-md-6">
		   						<div class="ourserve-sec">
					   				<div class="ourserve-cont other-serve">
					   					<p><i class="ti-icon ti-receipt"></i><div class="ourserve-bar"></div></p>
					   					<p class="font-20 skyblue"><b><?php the_field('our_services_section_1_title'); ?></b></p>
					   					<p class=""><?php the_field('our_services_section_1_content'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   					<div class="col-md-6">
		   						<div class="img-overflow border-radius-25">
		   							<img src="<?php echo $serveimg1['url']; ?>" alt="<?php echo $serveimg1['url']; ?>">
		   						</div>
		   					</div>
		   				</div>
		   			</div>
		   		</div><br><br><hr><br><br>
		   		<div class="row">
		   			<div class="col-md-12">
		   				<div class="row">
		   					<div class="col-md-6">
		   						<div class="img-overflow border-radius-25">
		   							<img src="<?php echo $serveimg2['url']; ?>" alt="<?php echo $serveimg2['url']; ?>">
		   						</div>
		   					</div>
		   					<div class="col-md-6">
		   						<div class="ourserve-sec">
					   				<div class="ourserve-cont">
					   					<p><div class="ourserve-bar"></div><i class="ti-icon ti-agenda"></i></p>
					   					<p class="font-20 skyblue"><b><?php the_field('our_services_section_2_title'); ?></b></p>
					   					<p class=""><?php the_field('our_services_section_2_content'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   				</div>
		   			</div>
		   		</div><br><br><hr><br><br>
		   		<div class="row">
		   			<div class="col-md-12">
		   				<div class="row">
		   					<div class="col-md-6">
		   						<div class="ourserve-sec list-before">
					   				<div class="ourserve-cont">
					   					<p><i class="ti-icon ti-rocket"></i><div class="ourserve-bar"></div></p>
					   					<p class="font-20 skyblue"><b><?php the_field('our_services_section_3_title'); ?></b></p>
					   					<p class=""><?php the_field('our_services_section_3_content'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   					<div class="col-md-6">
		   						<div class="img-overflow border-radius-25">
		   							<img src="<?php echo $serveimg3['url']; ?>" alt="<?php echo $serveimg3['url']; ?>">
		   						</div>
		   					</div>
		   				</div>
		   			</div>
		   		</div> <br><br>
<?php /* <hr><br><br>
		   		<div class="row">
		   			<div class="col-md-12">
		   				<div class="row">
		   					<div class="col-md-6">
		   						<div class="img-overflow border-radius-25">
		   							<img src="<?php echo $serveimg4['url']; ?>" alt="<?php echo $serveimg4['url']; ?>">
		   						</div>
		   					</div>
		   					<div class="col-md-6">
		   						<div class="ourserve-sec">
					   				<div class="ourserve-cont">
					   					<p><div class="ourserve-bar"></div><i class="ti-icon ti-agenda"></i></p>
					   					<p class="font-20 skyblue"><b><?php the_field('our_services_section_4_title'); ?></b></p>
					   					<p class=""><?php the_field('our_services_section_4_content'); ?></p>
					   				</div>
				   				</div>
		   					</div>
		   				</div>
		   			</div>
		   		</div> */ ?>
		   		
			</div>
		</section>

		<section class="padding-50-20 serve-2nd-sec blue-grey-background" id="billing-service">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center><!-- <p class="font-20">The difference is in the details</p>
			 			<hr class="hr-center"> -->
			 			<p class="font-25"><?php the_field('billing_service_title_1'); ?> <span class="skyblue"><b><?php the_field('billing_service_title_2'); ?></b></span></p>
							<hr class="hr-center">
			 			<?php /* <p class=""><i><?php the_field('billing_service_quotes'); ?></i></p>
			 			<p class=""><b><?php the_field('billing_service_quotes_author'); ?></b></p> */ ?></center>
					</div>
				</div><br>
			 	<div class="row">
			 		<div class="col-md-12">
			 			<?php the_field('billing_service_content_1'); ?>
			 		</div>
			 	</div><br><br>
		 		<div class="row" style="margin-right: 0px;">
		 			<!-- <div class="col-sm-4 col-md-4 col-lg-3">
		 				
		 			</div> -->
		 			<div class="col-md-12 bill-serve">
		 				<ul class="nav nav-pills nav-justified">
						    <li class="active"><a data-toggle="pill" href="#bill-serve1"><i class="ti-icon ti-email"></i><br><?php the_field('billing_service_tab_title_1'); ?></a></li>
						    <li><a data-toggle="pill" href="#bill-serve2"><i class="ti-icon ti-receipt"></i><br><?php the_field('billing_service_tab_title_2'); ?></a></li>
						    <li><a data-toggle="pill" href="#bill-serve3"><i class="ti-icon ti-pencil-alt"></i><br><?php the_field('billing_service_tab_title_3'); ?></a></li>
						    <li><a data-toggle="pill" href="#bill-serve4"><i class="ti-icon ti-target"></i><br><?php the_field('billing_service_tab_title_4'); ?></a></li>
						</ul>
		 				<div class="tab-content">
						    <div id="bill-serve1" class="tab-pane fade in active">
						      
							    <div class="row">
							      	<div class="col-sm-6">
							      		<img src="<?php echo $billimg1['url']; ?>" alt="<?php echo $billimg1['alt']; ?>">
							      	</div>
							      	<div class="col-sm-6">
							      		<?php the_field('billing_service_tab_content_1'); ?>
							      	</div>
							    </div>
						    </div>
						    <div id="bill-serve2" class="tab-pane fade">
						      
							    <div class="row">
							      	<div class="col-sm-6">
							      		<?php the_field('billing_service_tab_content_2'); ?>
							      	</div>
							      	<div class="col-sm-6">
							      		<img src="<?php echo $billimg2['url']; ?>" alt="<?php echo $billimg2['alt']; ?>">
							      	</div>
							    </div>
						    </div>
						    <div id="bill-serve3" class="tab-pane fade">
						      
							    <div class="row">
							      	<div class="col-sm-6">
							      		<img src="<?php echo $billimg3['url']; ?>" alt="<?php echo $billimg3['alt']; ?>">
							      	</div>
							      	<div class="col-sm-6">
							      		<?php the_field('billing_service_tab_content_3'); ?>
							      	</div>
							    </div>
						    </div>
						    <div id="bill-serve4" class="tab-pane fade list-before">
						        
						        <div class="row">
							      	<div class="col-sm-6">
							      		<?php the_field('billing_service_tab_content_4'); ?>
							      	</div>
							      	<div class="col-sm-6">
							      		<img src="<?php echo $billimg4['url']; ?>" alt="<?php echo $billimg4['alt']; ?>">
							      	</div>
						        </div>
						    </div>
						</div>
		 			</div>
		 		</div><br><br>
  
  				
			 	<div class="row">
			 		<div class="col-md-12">
			 			<?php the_field('billing_service_content_2'); ?>
			 		</div>
			 	</div>
			</div>
		</section>

		<?php /*

		<section class="padding-100-20" id="credentialing">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center>
						<p class="font-50 bottom-thi-animation"><?php the_field('credentialing_title_1'); ?> <span class="skyblue"><b><?php the_field('credentialing_title_2'); ?></b></span></p>
						<p class="bottom-sec-animation"><i><?php the_field('credentialing_quotes'); ?></i></p>
			 			<p class="bottom-fir-animation"><b><?php the_field('credentialing_quotes_author'); ?></b></p></center>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 zoom-in-animation">
						<?php the_field('credentialing_content'); ?>
					</div>
				</div>
			</div>
		</section>

		
		<section class="padding-100-20 serve-4th-sec" id="consulting" style="background-image: url('<?php echo $otherimg['url']; ?>'); ">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center>
			 			<p class="font-50"><?php the_field('other_services_title_1'); ?> <span class="skyblue"><b><?php the_field('other_services_title_2'); ?></b></span></p>
					</div>
				</div><br>
			 	<div class="row">
			 		<div class="col-md-12 zoom-in-animation">
			 			<?php the_field('other_services_content'); ?>
			 		</div>
			 		
			 	</div><br>
			</div>
		</section>
		*/ ?>
		<?php
			$credenimg = get_field('credentialing_background_image');
		?> 

		<section class="backimg-fixed" id="cred-service" style="background-image: url('<?php echo $credenimg['url']; ?>'); ">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 abt-staff">
						
					</div>
					<div class="col-md-6 back-white">
						<div class="padding-50-20">
							<div class="row">
								<div class="col-md-12">
									<center>
										<p class="font-25"><?php the_field('credentialing_title_1'); ?> <span class="skyblue"><b><?php the_field('credentialing_title_2'); ?></b></span></p>
										<hr class="hr-center">
										<?php /* <p class=""><i><?php the_field('credentialing_quotes'); ?></i></p>
							 			<p class=""><b><?php the_field('credentialing_quotes_author'); ?></b></p> */ ?>
							 		</center>
								</div>
							</div>
							<div class="row list-before">
								<div class="col-md-12">
									<?php the_field('credentialing_content'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php
			$otherimg = get_field('other_services_background_image');
		?>

		<section class="backimg-fixed" id="othe-service" style="background-image: url('<?php echo $otherimg['url']; ?>'); ">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 back-white">
						<div class="padding-50-20">
							<div class="row">
								<div class="col-md-12">
									<center>
									<p class="font-25"><?php the_field('other_services_title_1'); ?> <span class="skyblue"><b><?php the_field('other_services_title_2'); ?></b></span></p>
										<hr class="hr-center">
									</center>
								</div>
							</div><br>
							<div class="row list-before">
								<div class="col-md-12">
									<?php the_field('other_services_content'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 abt-staff">
						
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>