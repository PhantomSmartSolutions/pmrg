<?php
/*
Template Name: Tags
*/
get_header(); ?>

<style type="text/css">
	.pe-recent-posts-outer .image-top ul li img{
		width: 100%;
    	height: auto;
	}
	#cate-cont{ margin-top:60px; margin-bottom: 50px; }
	.pe-recent-posts-title-tag a{ color:black !important; }
	#blog-prev-next, #blog-prev-next a{
		color: #2fb4b4 !important;
    	font-weight: bold;
	}
	.posts{
		margin-bottom: 40px;
	}
</style>

<div id="container">
	<div id="content" role="main">

		<?php /*
		<?php the_post(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		
		<?php get_search_form(); ?>
		
		<h2>Archives by Month:</h2>
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
		
		<h2>Archives by Subject:</h2>
		<h1>Vengadesh</h1>
		<ul>
			 <?php wp_list_categories(); ?>
		</ul> */ ?>

		<div id="page-header">
				  
				<?php 
					 //    $terms = get_the_terms( $post->ID , 'categories' );
						// foreach ( $terms as $term ) {
               			?>
	            <div class="header-bg-parallax parallax-scroll" data-src="">
	                <div class="overlay">
	                    <div class="container text-center">
	                        <div class="header-description">
	                            <h1><?php the_title(); ?></h1>
	                            <div class="breadcrumbs">
	                                <ul>
	                                    <li><a href="<?php echo site_url(); ?>">Home</a></li>
	                                    <li><a href="javascript:void(0)">Categories</a></li>
	                                    <li><a href="javascript:void(0)" class="active"><?php echo the_title(); ?></a></li><?php // }
						?>
	                                </ul>
	                            </div>
	                            <!-- /header-small-nav -->
	                        </div>
	                        <!-- /header-description -->
	                    </div>
	                    <!-- /container -->
	                </div>
	                <!-- /overlay -->
	            </div>
	            <!-- /header-bg-parallax -->
	        </div>
	        <!-- Page Header End -->

		
 
		    
		   
		<div class="container" id="cate-cont">
			<div class="row">
				<div class="col-sm-9">
					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
					<div class="posts">
						<?php if (has_post_thumbnail( $post->ID ) ): ?>
					  	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
					  	<img src="<?php echo $image[0]; ?>" style="width: 100%;"><?php endif; ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					    <div class="entry-content">
					    	<?php the_content(); ?>
					    </div>
					    
               			<?php  /*
					    $terms = get_the_terms( $post->ID , 'categories' );
						foreach ( $terms as $term ) {
               			?>
               			<a href="<?php echo $term->link; ?>"><?php echo $term->name; ?></a>
               			<?php } */
						?>

					
					</div>
					<?php
						endwhile; endif;
						?>
				</div>
				<div class="col-sm-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>

		

	</div><!-- #content -->
</div><!-- #container -->


<?php get_footer(); ?>