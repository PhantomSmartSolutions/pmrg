<?php
	
	
	add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => 'Sidebar Widgets',
			'id'   => 'sidebar-widgets',
			'description'   => 'These are widgets for the sidebar.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));
	}

	/* Function to add menu to the theme */
	function wp_register_theme_menu() {
		register_nav_menu( 'primary', 'Main Navigation' );
	}
	add_action( 'init', 'wp_register_theme_menu' );

	function wp_register_theme_menu_mobile() {
		register_nav_menu( 'mobile', 'Mobile Menu' );
	}
	add_action( 'init', 'wp_register_theme_menu_mobile' );

	function wp_register_theme_menu_quicklinks() {
		register_nav_menu( 'quicklinks', 'Quick Links' );
	}
	add_action( 'init', 'wp_register_theme_menu_quicklinks' );

	function wp_register_theme_menu_footer() {
		register_nav_menu( 'footer', 'Footer Menu' );
	}
	add_action( 'init', 'wp_register_theme_menu_footer' );

	/* Function to check page/post has featured */
	function wp_theme_has_featured_posts() {
		return ! is_paged() && (bool) wp_theme_get_featured_posts();
	}

	/*To Add featured image*/
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'medium', 210, 210, array( 'left', 'top' ) ); // Hard crop left top


	/*Permalink by slug*/
	function get_permalink_by_slug( $slug ) {
		$obj = get_page_by_path( $slug );
		return get_permalink( $obj->ID );
	}

	/*Get page id by Slugs*/
	function get_ids_by_slugs($slugs) {
		 $slugs = preg_split("/,s?/", $slugs);
		 $ids = array();
		 foreach($slugs as $page_slug) {
			  $page = get_page_by_path($page_slug);
			  array_push($ids, $page->ID);
		 }
		 return implode(",", $ids);
	}

	// custom excerpt ellipses
	function new_excerpt_more( $more ) {
		return '<a href="'.get_permalink($post->ID).'" class="">'.'...'.'</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	
	// custom excerpt length
	function custom_excerpt_length( $length ) {
		return 55;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	// Numbered Pagination
	if ( !function_exists( 'wpex_pagination' ) ) {
		
		function wpex_pagination() {
			
			$prev_arrow = '&lt;';
			$next_arrow = '&gt;';
			
			global $wp_query;
			$total = $wp_query->max_num_pages;
			$big = 999999999; // need an unlikely integer
			if( $total > 1 )  {
				 if( !$current_page = get_query_var('paged') )
					 $current_page = 1;
				 if( get_option('permalink_structure') ) {
					 $format = 'page/%#%/';
				 } else {
					 $format = '&paged=%#%';
				 }
				echo paginate_links(array(
					'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'		=> $format,
					'current'		=> max( 1, get_query_var('paged') ),
					'total' 		=> $total,
					'mid_size'		=> 3,
					'type' 			=> 'list',
					'prev_text'		=> $prev_arrow,
					'next_text'		=> $next_arrow,
				 ) );
			}
		}
		
	}
	
	// get the the role object
	$role_object = get_role( 'editor' );
	
	// add $cap capability to this role object
	$role_object->add_cap( 'edit_theme_options' );
	
	//require_once( 'admin/theme-options.php' );
	function edit_page_title() {
		global $post, $title, $action, $current_screen;
	
		/*if($current_screen->post_type == 'sponsors' && $action != 'edit' && $current_screen->taxonomy != 'sponsors_categories') {
			$title = 'Add New Sponsors';           
		} else if($current_screen->post_type == 'events' && $action != 'edit' && $current_screen->taxonomy != 'events_categories') {
			$title = 'Add New Events';           
		} else if($current_screen->post_type == 'accolades' && $action != 'edit') {
			$title = 'Add New Accolades';           
		} else if($current_screen->post_type == 'artists' && $action != 'edit') {
			$title = 'Add New Artists';           
		} else {
			$title = $title;
		}*/
		return $title;  
	}
	add_action( 'admin_title', 'edit_page_title' );


function custom_post_type() {
 

    $labels = array(
        'name'                => _x( 'Resources', 'Post Type General Name', 'pmrg' ),
        'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'pmrg' ),
        'menu_name'           => __( 'Resources', 'pmrg' ),
        'parent_item_colon'   => __( 'Parent Resource', 'pmrg' ),
        'all_items'           => __( 'All Resources', 'pmrg' ),
        'view_item'           => __( 'View Resource', 'pmrg' ),
        'add_new_item'        => __( 'Add New Resource', 'pmrg' ),
        'add_new'             => __( 'Add New', 'pmrg' ),
        'edit_item'           => __( 'Edit Resource', 'pmrg' ),
        'update_item'         => __( 'Update Resource', 'pmrg' ),
        'search_items'        => __( 'Search Resource', 'pmrg' ),
        'not_found'           => __( 'Not Found', 'pmrg' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'pmrg' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Resources', 'pmrg' ),
        'description'         => __( 'PMRG Resources', 'pmrg' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    
     
    // Registering your Custom Post Type
    register_post_type( 'resources', $args );

    register_taxonomy( 'categories', array('resources'), array(
        'hierarchical' => true, 
        'label' => 'Categories', 
        'singular_label' => 'Category', 
        'rewrite' => array( 'slug' => 'categories', 'with_front'=> false )
        )
    );

    register_taxonomy_for_object_type( 'categories', 'resources' ); // Better be safe than sorry
 
}

add_action( 'init', 'custom_post_type', 0 );

	//create two taxonomies, genres and tags for the post type "tag"
	function create_tag_taxonomies() 
	{
	  // Add new taxonomy, NOT hierarchical (like tags)
	  $labels = array(
	    'name' => _x( 'Tags', 'taxonomy general name' ),
	    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Search Tags' ),
	    'popular_items' => __( 'Popular Tags' ),
	    'all_items' => __( 'All Tags' ),
	    'parent_item' => null,
	    'parent_item_colon' => null,
	    'edit_item' => __( 'Edit Tag' ), 
	    'update_item' => __( 'Update Tag' ),
	    'add_new_item' => __( 'Add New Tag' ),
	    'new_item_name' => __( 'New Tag Name' ),
	    'separate_items_with_commas' => __( 'Separate tags with commas' ),
	    'add_or_remove_items' => __( 'Add or remove tags' ),
	    'choose_from_most_used' => __( 'Choose from the most used tags' ),
	    'menu_name' => __( 'Tags' ),
	  ); 

	  register_taxonomy('tags', 'resources', array(
	    'hierarchical' => false,
	    'labels' => $labels,
	    'show_ui' => true,
	    'update_count_callback' => '_update_post_term_count',
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'tags' ),
	  ));
	}


add_action( 'init', 'create_tag_taxonomies', 0 );

function wpb_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Subscribe Widget Area',
        'id'            => 'custom-header-widget',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="chw-title">',
        'after_title'   => '</h2>',
    ) );
 
}
add_action( 'widgets_init', 'wpb_widgets_init' );

?>