<?php
/*
Template Name: Categories
*/
get_header(); ?>

<style type="text/css">

</style>
		
 		<section class="blue-gradient padding-100-20">
 			<?php 
			    $terms = get_the_terms( $post->ID , 'categories' );
				foreach ( $terms as $term ) {
       			?>
       			<center>
       				<div class="breadcrumbs">
                        <ul>
                            <li><a href="<?php echo site_url(); ?>">Home</a></li> &nbsp; / &nbsp; 
                            <li><a href="javascript:void(0)">Categories</a></li> &nbsp; / &nbsp; 
                            <li><a href="javascript:void(0)" class="active"><?php echo $term->name; ?></a></li><?php }
							?>
                        </ul>
                    </div>
       			</center>
 		</section>
		    
		   
		<?php 
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array( 'post_type' => 'blogs', 'paged' => $paged, 'order' => "DESC" );
$loop = new WP_Query( $args ); ?>
	<section class="blue-grey-background padding-50-20">
	<div class="container">
		<!-- <div class="row"> -->
			<?php 
				$i = 1;
				if(have_posts()) : while(have_posts()) : the_post(); ?>
					<?php if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 9 || $i == 11 || $i == 13 || $i == 15 || $i == 17 ) { ?>
					<div class="row library-blogs"><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');?>
						<div class="col-sm-6 no-padding blog-image" style="background-image: url('<?php echo $thumb['0']; ?>');background-repeat: no-repeat; background-size: cover;height: 400px; ">
							
							<div class="img-overflow">
								<div class="blog-arrow"></div>
								<!-- <a href="<?php the_permalink(); ?>">
						   			<img src="">
						   		</a> -->
							</div>
						</div>
						<div class="col-sm-6">
							<div class="blog-caption">
								<center>
									<a href="<?php the_permalink(); ?>"><h3 class="text-center"><?php the_title(); ?></h3></a>
									<center><hr class="hr-center"></center>
								    <div style="text-align: justify;">
								    	<?php 
								    		$title = get_the_title();
										    $content = get_the_content();
										    echo wp_trim_words( $content , '30' );  
										?>
									</div>
									<?php 
											    $terms = get_the_terms( $post->ID , 'categories' );
												foreach ( $terms as $term ) {
													$link = get_term_link($term);
						               			?><br>
												<span class='post-labels'>
													<a href="<?php echo $link; ?>" id="cate-name-link"><i class="fa fa-folder-open-o"></i> <?php echo $term->name; ?></a>
												</span>
												<?php
												}
													?>
								</center>
							</div>
							
						</div>
					</div><br><br>

					<?php
					      	
					      	
					      	// } else{
					       }  if( $i == 2 || $i == 4 || $i == 6 || $i == 8 || $i == 10 || $i == 12 || $i == 14 || $i == 16 ) {
					      	?>

					<div class="row library-blogs">
						
						<div class="col-sm-6">
							<div class="blog-caption">
								<center>
									<a href="<?php the_permalink(); ?>"><h3 class="text-center"><?php the_title(); ?></h3></a>
									<center><hr class="hr-center"></center>
								    <div style="text-align: justify;">
								    	<?php 
								    		$title = get_the_title();
										    $content = get_the_content();
										    echo wp_trim_words( $content , '30' );  
										?>
									</div>
									<?php 
											    $terms = get_the_terms( $post->ID , 'categories' );
												foreach ( $terms as $term ) {
													$link = get_term_link($term);
						               			?><br>
												<span class='post-labels'>
													<a href="<?php echo $link; ?>" id="cate-name-link"><i class="fa fa-folder-open-o"></i> <?php echo $term->name; ?></a>
												</span>
												<?php
												}
													?>
								</center>
							</div>
							
						</div>
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');?>
						<div class="col-sm-6 no-padding blog-image" style="background-image: url('<?php echo $thumb['0']; ?>');background-repeat: no-repeat; background-size: cover;height: 400px; ">
							
							<div class="img-overflow">
								<div class="blog-arrow"></div>
								<!-- <a href="<?php the_permalink(); ?>">
						   			<img src="<?php echo $thumb['0']; ?>">
						   		</a> -->
							</div>
						</div>
					</div><br><br>

			

			<?php
			}
					      	 $i++;
				endwhile; 
			?>
			 <?php if ($loop->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
            	<div>
            		<p id="blog-prev-next">
            			<span>
                        	<?php echo get_previous_posts_link( '&laquo; Previous Page' ); // display newer posts link ?>
                        </span>
                        <span style="float: right;">
                        <?php echo get_next_posts_link( 'Next Page &raquo;', $query->max_num_pages ); // display older posts link ?>
                    	</span>
                    </p>
                </div>
        <?php } ?>
			<?php

			endif; 
				?>
			
		<!-- </div> -->
	</div>
</section>		


<?php get_footer(); ?>