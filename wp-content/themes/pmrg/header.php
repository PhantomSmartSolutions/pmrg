<!DOCTYPE html>
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php /* <title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title> */ ?>
	<?php 
        if ( is_front_page() ) {
            ?>
                <title>Home - PMRG</title>
            <?php
       	} 
        else {
           ?>

        <title><?php is_front_page() ? bloginfo('description') : wp_title(''); ?> - <?php bloginfo('name'); ?></title>
        <?php
       }
    ?>
    <?php
    if (is_page('resources')) {
       		?>
                <title>Library</title>
            <?php
       	} ?>
	<title><?php the_title(); ?> - <?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
   	<!-- <link rel="shortcut icon" href="<?php // echo get_template_directory_uri(); ?>/assets/images/favicon.ico" /> -->
    <!-- Hiding the Browser's User Interface for iOS & Android -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="mobile-web-app-capable" content="yes">  
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/icons/themify-icons/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/slick-style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/onload-animation.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/slider/css/rotate-3d-style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/desktop-timeline.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/icons/streamline-icon/css/streamline-icon.css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"> -->
	<link rel="stylesheet" id="medicare-style-css"  href="<?php echo get_template_directory_uri(); ?>/assets/medicare/css/style4d2c.css?ver=5.2.4" type="text/css" media="screen" />  

	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?> id="body">
                <!-- <div class="header_menu"><?php 
                      // wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_id' => '', 'menu_class'=> '') ); ?>
				</div> -->

				<?php /*
				First use the method, if it not work use the above method.
				<html nav class id>
					<?php 
					$args = array(
						'theme_location' => 'primary'
					);
				?>
				<ul class="sidebar-menu err_hand">
				<?php wp_nav_menu( $args ); ?>
				</ul>
				</html nav class id> */
				?>

	<header class="header">
		<div class="container-fluid" id="desktop-nav">
		    <!-- <div class="logo"><a href="index.html"><img src="<?php // echo get_template_directory_uri(); ?>/assets/img/PMRG-Logo195.png"></a></div> -->
		    <div class="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo ot_get_option('main_menu_logo'); ?>"></a></div>
		    <div class="menu-overlay">
		    	<div id="desktop-menu">
		    		<?php 
                        $args = array(
                            'theme_location' => 'primary'
                        );
                    ?>
			        <!-- <ul> -->
				        <!-- <li><a href="index.html">Home</a>
				        	<div class="arrow-up"></div>
				        </li>

				        <li>
				            <a href="who-we-are.html">Who We Are &#9662;</a>
				            <div class="arrow-up"></div>
				            <ul class="dropdown">
				                <li><a href="who-we-are.html"><span class="fa fa-angle-right"></span> &nbsp Management Team</a></li>
				                <li><a href="who-we-are.html"><span class="fa fa-angle-right"></span> &nbsp Our Staff</a></li>
				                <li><a href="who-we-are.html"><span class="fa fa-angle-right"></span> &nbsp Our History</a></li>
				                <li><a href="who-we-are.html"><span class="fa fa-angle-right"></span> &nbsp Our Perspective</a></li>
				            </ul>
				        </li>
				        <li>
				            <a href="our-services.html">Our Services &#9662;</a>
				            <div class="arrow-up"></div>
				            <ul class="dropdown">
				                <li><a href="our-services.html"><span class="fa fa-angle-right"></span> &nbsp Billing Services</a></li>
				                <li><a href="our-services.html"><span class="fa fa-angle-right"></span> &nbsp Credentialing</a></li>
				                <li><a href="our-services.html"><span class="fa fa-angle-right"></span> &nbsp Other Services</a></li>
				            </ul>
				        </li>
				        <li><a href="our-clients.html">Our Clients</a>
				        	<div class="arrow-up"></div>
				        </li>
				        <li class="active">
				            <a href="library.html">Library &#9662;</a>
				            <div class="arrow-up"></div>
				            <ul class="dropdown">
				                <li><a href="q4-2019-newsletter.html"><span class="fa fa-angle-right"></span> &nbsp Q4-2019 Newsletter</a></li>
				                <li><a href="q3-2019-newsletter.html"><span class="fa fa-angle-right"></span> &nbsp Q3-2019 Newsletter</a></li>
				                <li><a href="q2-2019-newsletter.html"><span class="fa fa-angle-right"></span> &nbsp Q2-2019 Newsletter</a></li>
				                <li><a href="q1-2019-newsletter.html"><span class="fa fa-angle-right"></span> &nbsp Q1-2019 Newsletter</a></li>
				                <li><a href="q4-2018-newsletter.html"><span class="fa fa-angle-right"></span> &nbsp Q4-2018 Newsletter</a></li>
				                <li><a href="managing-practice-financial-performance.html"><span class="fa fa-angle-right"></span> &nbsp Q3-2018 Newsletter</a></li>
				                <li><a href="charge-capture.html"><span class="fa fa-angle-right"></span> &nbsp Charge Capture</a></li>
				            </ul>
				        </li>
				        <li><a href="contact-us.html">Contact Us</a><div class="arrow-up"></div></li> -->
				        <?php wp_nav_menu( $args ); ?>
				    <!-- </ul> -->

				</div>
				<div class="topBarInMenu">
					<div class="topBarInMenuCell">
						<div class="btTopBox widget_search">
							<div class="btSearch">
	        					<div class="btSearchInner" role="search">
	                				<div class="btSearchInnerContent">
	                        			<form action="#" method="get">
	                        				<input type="text" name="s" placeholder="Looking for..." class="untouched">
	                        				<button type="submit" data-icon="&#xf105;"></button>
	                        			</form>
	                				</div>
	        					</div>
	        				</div>
	        			</div>
	        			<a href="<?php echo ot_get_option('facebook_link'); ?>" target="_blank" class="btIconWidget ">
	        				<span class="btIconWidgetIcon">
	        					<span class="btIco " >
	        						<!-- <span  data-ico-fa="&#xf09a;" class="btIcoHolder"><em></em></span> -->
	        						<span class="fa fa-facebook"></span>
	        					</span>
	        				</span>
	        			</a>
	        			<a href="<?php echo ot_get_option('twitter_link'); ?>" target="_blank" class="btIconWidget ">
	        				<span class="btIconWidgetIcon">
	        					<span class="btIco " >
	        						<!-- <span  data-ico-fa="&#xf099;" class="btIcoHolder"><em></em></span> -->
	        						<span class="fa fa-twitter"></span>
	        					</span>
	        				</span>
	        			</a>
	        			<a href="<?php echo ot_get_option('linkedin_link'); ?>" target="_blank" class="btIconWidget ">
	        				<span class="btIconWidgetIcon">
	        					<span class="btIco " >
	        						<!-- <span  data-ico-fa="&#xf0e1;" class="btIcoHolder"><em></em></span> -->
	        						<span class="fa fa-linkedin"></span>
	        					</span>
	        				</span>
	        			</a>
	        		</div><!-- /topBarInMenu -->
				</div><!-- /topBarInMenuCell -->
		    </div>
		    
		</div>
		
	</header>
	<div id="mobile-menu">
	  	<div class="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo ot_get_option('mobile_menu_logo'); ?>" id="mobile-logo"></a></div>
	  	<button id="menu-butt" onclick="myFunction(this)" class="">
	  		<center>
	  			<div class="bar1"></div>
	  			<div class="bar2"></div>
	  			<div class="bar3"></div>
	  			<!-- <p style="font-size:13px !important;">MENU</p> -->
	  		</center> 
	  	</button>
	  	<div id="mySidenav" class="sidenav">
	  		<div class="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo ot_get_option('mobile_menu_logo'); ?>"></a></div>
	  		<div class="mobile-topBarInMenu">
				<div class="mobile-topBarInMenuCell">
					<div class="mobile-btTopBox widget_search">
						<div class="mobile-btSearch">
        					<div class="mobile-btSearchInner" role="search">
                				<div class="mobile-btSearchInnerContent">
                        			<form action="#" method="get">
                        				<input type="text" name="s" placeholder="Looking for..." class="untouched">
                        				<button type="submit" data-icon=""></button>
                        			</form>
                				</div>
        					</div>
        				</div>
        			</div><center>
        			<a href="<?php echo ot_get_option('facebook_link'); ?>" target="_blank" class="mobile-btIconWidget ">
        				<span class="btIconWidgetIcon">
        					<span class="btIco ">
        						<span class="fa fa-facebook"></span>
        					</span>
        				</span>
        			</a>
        			<a href="<?php echo ot_get_option('twitter_link'); ?>" target="_blank" class="mobile-btIconWidget ">
        				<span class="btIconWidgetIcon">
        					<span class="btIco ">
        						<span class="fa fa-twitter"></span>
        					</span>
        				</span>
        			</a>
        			<a href="<?php echo ot_get_option('linkedin_link'); ?>" target="_blank" class="mobile-btIconWidget ">
        				<span class="btIconWidgetIcon">
        					<span class="btIco ">
        						<span class="fa fa-linkedin"></span>
        					</span>
        				</span>
        			</a></center>					
        		</div><!-- /topBarInMenu -->
			</div>
		    <!-- <a class="active" href="index.html">Home</a>
		    <a href="javascript:void()" class="dropdown-btn">Who We Are 
				<i class="fa fa-caret-down"></i>
			</a>
			<div class="dropdown-container">
				<a href="who-we-are.html">Management Team</a>
				<a href="who-we-are.html">Our Staff</a>
				<a href="who-we-are.html">Our History</a>
				<a href="who-we-are.html">Our Perspective</a>
			</div>
			<a href="javascript:void()" class="dropdown-btn">Our Services 
				<i class="fa fa-caret-down"></i>
			</a>
			<div class="dropdown-container">
				<a href="our-services.html">Billing Services</a>
				<a href="our-services.html">Credentialing</a>
				<a href="our-services.html">Other Services</a>
			</div>
		  	<a href="our-clients.html">Our Clients</a>
		  	<a href="javascript:void()" class="dropdown-btn">Library 
				<i class="fa fa-caret-down"></i>
			</a>
			<div class="dropdown-container">
				<a href="library.html" class="dropdown-btn">Library</a>
				<a href="q4-2019-newsletter.html">Q4-2019 Newsletter</a>
				<a href="q3-2019-newsletter.html">Q3-2019 Newsletter</a>
				<a href="q2-2019-newsletter.html">Q2-2019 Newsletter</a>
				<a href="q1-2019-newsletter.html">Q1-2019 Newsletter</a>
				<a href="q4-2018-newsletter.html">Q4-2018 Newsletter</a>
				<a href="managing-practice-financial-performance.html">Q3-2018 Newsletter</a>
				<a href="charge-capture.html">Charge Capture</a>
			</div>
		  	<a href="contact-us.html">Contact Us</a> -->
		  	<?php 
                $args = array(
                    'theme_location' => 'mobile'
                );
            ?>
            <?php wp_nav_menu( $args ); ?>
		</div>
	</div>

	<div class="main-container">