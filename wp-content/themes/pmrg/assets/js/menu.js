$(document).ready(function() {
  $("#menu-butt").click(function(){
    $("#mySidenav").toggleClass("main");
    // $("#body").toggleClass("main2");
    $("#menu-butt").toggleClass("main2");
  });

  $("#management-team-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#management-team").offset().top
    }, 1000);
});

$("#our-staff-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#our-staff").offset().top
    }, 1000);
});

$("#our-history-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#our-history").offset().top
    }, 1000);
});

$("#our-perspective-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#our-perspective").offset().top
    }, 1000);
});

$("#billing-service-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#billing-service").offset().top
    }, 1000);
});

$("#credentialing-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#credentialing").offset().top
    }, 1000);
});

$("#consulting-butt").click(function() {
    $("html, body").animate({
        scrollTop: $("#consulting").offset().top
    }, 1000);
});

});

function myFunction(x) {
  x.classList.toggle("change");
}

/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
// var dropdown = document.getElementsByClassName("dropdown-btn");
// var i;

// for (i = 0; i < dropdown.length; i++) {
//   dropdown[i].addEventListener("click", function() {
//   this.classList.toggle("active");
//   var dropdownContent = this.nextElementSibling;
//   if (dropdownContent.style.display === "block") {
//   dropdownContent.style.display = "none";
//   dropdownContent.style.transition = "1s";
//   } else {
//   dropdownContent.style.display = "block";
//   dropdownContent.style.transition = "1s";
//   }
//   });
// }

var dropdown = document.getElementsByquerySelector(".menu-item-has-children a");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = document.getElementsByquerySelector("#menu-mobile-menu li .sub-menu");
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  // dropdownContent.style.height = "0px";
  dropdownContent.style.transition = "1s";
  } else {
  dropdownContent.style.display = "block";
  // dropdownContent.style.height = "auto";
  dropdownContent.style.transition = "1s";
  }
  });
}



