(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

var win = $(window);

/* -------------  Animation left to Right -------------------- */

var allModslef1 = $(".left-fir-animation");

allModslef1.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModslef1.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("left-animate1"); 
    } 
  });
  
});
var allModslef2 = $(".left-sec-animation");

allModslef2.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModslef2.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("left-animate2"); 
    } 
  });
  
});
var allModslef3 = $(".left-thi-animation");

allModslef3.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModslef3.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("left-animate3"); 
    } 
  });
  
});
var allModslef4 = $(".left-fou-animation");

allModslef4.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModslef4.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("left-animate4"); 
    } 
  });
  
});

/* ---------------- Right to Left Animation ------------------ */

var allModsrig1 = $(".right-fir-animation");

allModsrig1.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsrig1.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("right-animate1"); 
    } 
  });
  
});

var allModsrig2 = $(".right-sec-animation");

allModsrig2.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsrig2.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("right-animate2"); 
    } 
  });
  
});

var allModsrig3 = $(".right-thi-animation");

allModsrig3.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsrig3.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("right-animate3"); 
    } 
  });
  
});

var allModsrig4 = $(".right-fou-animation");

allModsrig4.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsrig4.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("right-animate4"); 
    } 
  });
  
});

/* ----------------- Bottom to Top Animation --------------- */

var allModstop1 = $(".top-fir-animation");

allModstop1.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModstop1.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("top-animate1"); 
    } 
  });
  
});
var allModstop2 = $(".top-sec-animation");

allModstop2.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModstop2.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("top-animate2"); 
    } 
  });
  
});
var allModstop3 = $(".top-thi-animation");

allModstop3.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModstop3.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("top-animate3"); 
    } 
  });
  
});
var allModstop4 = $(".top-fou-animation");

allModstop4.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModstop4.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("top-animate4"); 
    } 
  });
  
});


/* ----------------- Bottom to Top Animation --------------- */

var allModsbot1 = $(".bottom-fir-animation");

allModsbot1.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsbot1.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("bottom-animate1"); 
    } 
  });
  
});
var allModsbot2 = $(".bottom-sec-animation");

allModsbot2.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsbot2.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("bottom-animate2"); 
    } 
  });
  
});
var allModsbot3 = $(".bottom-thi-animation");

allModsbot3.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsbot3.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("bottom-animate3"); 
    } 
  });
  
});
var allModsbot4 = $(".bottom-fou-animation");

allModsbot4.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModsbot4.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("bottom-animate4"); 
    } 
  });
  
});

/* ------------------ Zoom in Animation -------------------- */

var allModszoomin = $(".zoom-in-animation");

allModszoomin.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allModszoomin.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("scale-zoom-in"); 
    } 
  });
  
});


// var allModss = $("section:nth-child(even) .col-sm-3, section:nth-child(even) .col-sm-4, section:nth-child(even) .col-sm-6, section:nth-child(even) .col-sm-12, section:nth-child(even) .col-md-3, section:nth-child(even) .col-md-4, section:nth-child(even) .col-md-6, section:nth-child(even) .col-md-12, section:nth-child(even) .col-xs-3, section:nth-child(even) .col-xs-4, section:nth-child(even) .col-xs-6, section:nth-child(even) .col-xs-12");

// allModss.each(function(i, el) {
//   var el = $(el);
//   if (el.visible(true)) {
//     el.addClass("already-visible-even"); 
//   } 
// });

// win.scroll(function(event) {
  
//   allModss.each(function(i, el) {
//     var el = $(el);
//     if (el.visible(true)) {
//       el.addClass("come-in-even"); 
//     } 
//   });
  
// });

$(document).ready(function () {
    $(".olanimate").fadeIn(1000).removeClass('hidden');
});