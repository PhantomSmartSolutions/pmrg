
	<?php wp_footer(); ?>
	
	<!-- Don't forget analytics -->

	<footer class="background-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/footer2.png');">
		<div class="footer-opacity" style="background-color: rgba(255,255,255,0.9);">
		<div class="container footer-cont">
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="<?php echo ot_get_option('footer_logo'); ?>">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4" id="footer-col-1">
					<h3 class="skyblue"><?php echo ot_get_option('footer_column_one_title'); ?></h3>
					<p><?php echo ot_get_option('footer_content'); ?></p>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-3" id="footer-col-2">
					
					<h3 class="skyblue"><?php echo ot_get_option('footer_column_two_title'); ?></h3>
					<div class="row">
						
						<div class="col-md-12">
							<?php 
				                $args = array(
				                    'theme_location' => 'quicklinks'
				                );
				            ?>
				            <?php wp_nav_menu( $args ); ?>
						</div>
						
					</div>
				</div>
				
				<div class="col-md-4" id="footer-col-3">
					<h3 class="skyblue"><?php echo ot_get_option('footer_column_three_title'); ?></h3>
					<p><?php echo ot_get_option('footer_column_content'); ?></p>

					<?php
 
					if ( is_active_sidebar( 'custom-header-widget' ) ) : ?>
					    <div id="header-widget-area" class="chw-widget-area widget-area" role="complementary">
					    <?php dynamic_sidebar( 'custom-header-widget' ); ?>
					    </div>
					     
					<?php endif; ?>
				</div>
			</div>

		</div>
		<div class="copyright-section">
			<div class="container"><hr>
				<div class="row">
					<div class="col-md-6 footer-last-col1">
						<?php echo ot_get_option('copyright_content'); ?>
					</div>
					<div class="col-md-6 footer-last-col2">
						<?php 
			                $args = array(
			                    'theme_location' => 'footer'
			                );
			            ?>
			            <?php wp_nav_menu( $args ); ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</footer>
	<div id="stop" class="scrollTop">
		<span><a href=""><i class="fa fa-chevron-up"></i></a></span>
    </div>

	<style>
		.scrollTop {
		  position: fixed;
		  right: 40px;
		  bottom: 20px;
		  background-color: #00b3cc;
		  padding: 20px;
		  opacity: 0;
		  transition: all 0.4s ease-in-out 0s;
		  border-radius:50%;
		}

		.scrollTop a {
		  font-size: 18px;
		  color: #fff;
		}
		#header-widget-area .tnp-field-button .tnp-submit{ background-color: #337ab7; font-size: 18px; }
	</style>

	<script>
		$(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  });
		});
</script>

<script type="text/javascript">
	document.querySelector("#header-widget-area .tnp-field-email .tnp-email").placeholder = "Type in your email address";
	$(document).ready(function () {
		// $("#header-widget-area .tnp-field-button .tnp-submit").addClass("btn");
		// $("#header-widget-area .tnp-field-button .tnp-submit").addClass("btn-primary");
	});
</script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/scroll.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/menu.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/medicare/js/slick.min4d2c.js?ver=5.2.4"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/medicare/js/sliders4d2c.js?ver=5.2.4"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/faq-script.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/onload-animation.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick-script.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/slider/js/rotate-3d-script.js"></script>
	<!-- <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/cubeportfolio.init.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/other.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/script.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/sticky-kit.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/custom.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/jquery.validate.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/jquery.unobtrusive-ajax.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/vertex/js/jquery-shims.js"></script> -->
	
</body>

</html>
