

<?php get_header(); ?>

<style type="text/css">
	#commentform{
		padding: 25px;
	    border: 1px solid #e1e1e1;
	    box-shadow: 2px 2px 2px 2px #eee;
	}
	#commentform #comment{
		width: 100%;
	    height: 140px;
	    border: 1px solid #ced4da;
	    box-shadow: 2px 2px 2px 2px #eee;
	}
	#commentform #submit{
		height: 40px;
	    border: 1px solid #2fb4b4;
	    margin-top: 20px;
	    background-color:#2fb4b4;
	    color:white;
	}
	.pe-recent-posts-outer .image-top ul li img{
		width: 100%;
    	height: auto;
	}
	#blog-cont{ margin-top:60px; margin-bottom: 50px; }
	h1, h2, h3, h4, h5, h6, p, a, li{ font-family: Poppins, sans-serif;  }
	.widget.PE_Recent_Posts h2{ margin: 20px 0; font-weight: bold; }
	#myCarousel-pe_recent_posts-2 h5{ font-weight: bold; }
	#myCarousel-pe_recent_posts-2 .item ul{ margin-bottom: 25px; }
	.pe-recent-posts-title-tag a{ color:black !important; }
	#blog-prev-next, #blog-prev-next a{
		color: #000 !important;
    	font-weight: bold;
	}

	#commentform #author, #commentform #email, #commentform #url{
	    margin-bottom: 20px;
	    height: calc(2.25rem + 2px);
	    padding: .375rem .75rem;
	    font-size: 1rem;
	    font-weight: 400;
	    line-height: 1.5;
	    color: #495057;
	    background-color: #fff;
	    border: 1px solid #ced4da;
	    border-radius: .25rem;
	    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	    margin-right: 10px;
	}
	#cate-name-link{
		color: #000 !important;
		font-weight: bold;
	}
	.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
		background-color: #30b2d2;
		color: white;
		border-radius: 0px;
	}
	.nav-tabs>li>a {
		font-size: 14px;
		color: #666;
		border-radius: 0px;
		background-color: #e1e1e1;
	}
	.nav-tabs {
		border-bottom: 0px solid #ddd !important;
	}
	.blog-tab-cont{
		border: 1px solid #e1e1e1;
    	padding: 15px 25px;
	}
	.nav-pills>li>a:hover {
		background-color: #0b6bb5 !important;
		color: white;
		border-radius: 0px;
	}
	.blog-tab-ul li{ padding-left: 0px !important;
		padding-top: 0px !important; }
	.blog-breadcrumbs li a{ color:black !important; }
</style>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<?php /*
			<div id="page-header">

				<?php if (has_post_thumbnail( $post->ID ) ): ?>
				  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				  
				
	            <div class="header-bg-parallax parallax-scroll" data-src="<?php echo $image[0]; ?>"><?php endif; ?>
	                
	            </div>
	            <!-- /header-bg-parallax -->
	        </div>
	        <!-- Page Header End --> */ ?>
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	        <section class="background-img padding-150-30" style="background-image:url('<?php echo $image[0]; ?>');"><?php endif; ?>
				<center>
					<p class="font-20">Library</p>
					<hr class="hr-center">
					<p class="font-25"><?php the_title(); ?></p>
					<?php /* <ul class="blog-breadcrumbs">
						<li><a href="<?php echo site_url(); ?>">Home</a></li> &nbsp / &nbsp 
						<li><a href="<?php echo site_url(); ?>/?post_type=resources">Library</a></li>
					</ul> */ ?>
				</center>
				
			</section>

			<div class="container entry library-content"><br><br><br>

				<div class="row">
					<div class="col-sm-12 list-before" style="text-align: justify;">
						<?php the_content(); ?>
						<br>
						<ul class="nav nav-tabs blog-tab-ul">
						    <li class="active"><a data-toggle="tab" href="#blog-sec1"><?php the_field('tab_section_title_1'); ?></a></li>
							<?php 
					      	$i = 2;
					      	if( have_rows('tab_section_titles') ):

						 	// loop through the rows of data
						    while ( have_rows('tab_section_titles') ) : the_row();
					      ?>
						    <li><a data-toggle="tab" href="#blog-sec<?php echo $i; ?>"><?php the_sub_field('tab_section_title'); ?></a></li>
						   <?php
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>
						</ul>
						<div class="tab-content blog-tab-cont">
						    <div id="blog-sec1" class="tab-pane fade in active">
							      <?php the_field('tab_section_content_1'); ?>
						    </div>
						    <?php 
					      	$i = 2;
					      	if( have_rows('tab_section_contents') ):

						 	// loop through the rows of data
						    while ( have_rows('tab_section_contents') ) : the_row();
					      ?>
						    <div id="blog-sec<?php echo $i; ?>" class="tab-pane fade">
							    <?php the_sub_field('tab_section_content'); ?>
						    </div>
						    <?php
					      	 $i++;
					        endwhile;

							else :

							    // no rows found

							endif;

							?>

						</div>
						<br>
						<?php the_field('main_content_2'); ?>
						<br>
						<?php /* 
					    $terms = get_the_terms( $post->ID , 'categories' );
						foreach ( $terms as $term ) {
							$link = get_term_link($term);
							$name = $term->name;
               			?>
               			<a href="<?php echo $link; ?>" id="cate-name-link"><i class="fa fa-folder-open-o"></i> &nbsp&nbsp <?php echo $name ?></a>
               			<?php } */
						?>
						<br>
						<?php // wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
						
						<?php // the_tags( 'Tags: ', ', ', ''); ?>

						<?php // edit_post_link('Edit this entry','','.'); ?>

						<?php // include (TEMPLATEPATH . '/inc/meta.php' ); ?>

						<?php comments_template(); ?><br>

						<!-- <h4>Categories</h4> -->

						<div><p id="blog-prev-next"><span><?php previous_post_link(); ?></span>    <span style="float: right;"><?php next_post_link(); ?></span></p></div>
						
					</div>
					
				</div> <br>

			</div>

	<?php endwhile; endif; ?>
	


<?php get_footer(); ?>