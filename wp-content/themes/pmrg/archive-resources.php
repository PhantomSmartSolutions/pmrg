<?php
/*
Template Name: Resources Archives
*/
get_header(); ?>
<style>
	#blog-prev a, #blog-next a{ color:white !important; font-weight:bold;
    font-weight: bold;
    background-color: #30b2d2;
    height: 40px;
    width: 150px;
    text-align: center;
    padding: 8px 20px;
    border-radius: 3px; }

	#menu-main-menu #menu-item-441 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:bold; }
	#menu-main-menu #menu-item-441 ul li a{ border-bottom: 0px solid transparent; background-color: transparent; color: #30b2d2; font-weight:normal; }
	header.fixed #menu-main-menu #menu-item-441 a{ border-bottom: 3px solid #30b2d2; background-color: transparent; color: #30b2d2; font-weight:normal; }
</style>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/lily-style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/lilystyle.css">
 -->

	<section class="background-img padding-150-30" style="background-image:url('<?php echo ot_get_option('library_banner_image'); ?>');">
		<div class="container">
			<center>
			<p class="font-20">Library</p>
			<hr class="hr-center">
			<p class="font-14">Resources</p></center>
		</div>
		
	
	</section>
		<?php 
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array( 'post_type' => 'resources', 'paged' => $paged, 'order' => "DESC" );
$loop = new WP_Query( $args ); ?>

<section class="blue-grey-background padding-50-20">
	<div class="container">
		<p><?php echo ot_get_option('library_banner_content'); ?></p><br>
		<!-- <div class="row"> -->
			<?php 
				$i = 1;
				if(have_posts()) : while(have_posts()) : the_post(); ?>
					<?php if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 9 || $i == 11 || $i == 13 || $i == 15 || $i == 17 ) { ?>
					<div class="row library-blogs"><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');?>
						<div class="col-sm-6 no-padding blog-image" style="background-image: url('<?php echo $thumb['0']; ?>');background-repeat: no-repeat; background-size: cover;height: 400px; ">
							
							<div class="img-overflow">
								<div class="blog-arrow"></div>
								<!-- <a href="<?php the_permalink(); ?>">
						   			<img src="">
						   		</a> -->
							</div>
						</div>
						<div class="col-sm-6">
							<div class="blog-caption">
								<center>
									<h3 class="text-center skyblue"><?php the_title(); ?></h3>
									<center><hr class="hr-center"></center>
								    <div style="text-align: justify;font-size:13px;letter-spacing:1px;">
								    	<?php 
								    		$title = get_the_title();
										    $content = get_the_content();
										    echo wp_trim_words( $content , '30' );  
										?><br>
										<a href="<?php the_permalink(); ?>" style="float:right;font-weight:bold;">Read More</a>
									</div>
									<?php /*
											    $terms = get_the_terms( $post->ID , 'categories' );
												foreach ( $terms as $term ) {
													$link = get_term_link($term);
						               			?><br>
												<span class='post-labels'>
													<a href="<?php echo $link; ?>" id="cate-name-link"><i class="fa fa-folder-open-o"></i> <?php echo $term->name; ?></a>
												</span>
												<?php
												} */
													?>
								</center>
							</div>
							
						</div>
					</div><br><br>

					<?php
					      	
					      	
					      	// } else{
					       }  if( $i == 2 || $i == 4 || $i == 6 || $i == 8 || $i == 10 || $i == 12 || $i == 14 || $i == 16 ) {
					      	?>

					<div class="row library-blogs">
						
						<div class="col-sm-6">
							<div class="blog-caption">
								<center>
									<h3 class="text-center skyblue"><?php the_title(); ?></h3>
									<center><hr class="hr-center"></center>
								    <div style="text-align: justify;font-size:13px;letter-spacing:1px;">
								    	<?php 
								    		$title = get_the_title();
										    $content = get_the_content();
										    echo wp_trim_words( $content , '30' );  
										?><br>
										<a href="<?php the_permalink(); ?>" style="float:right;font-weight:bold;">Read More</a>
									</div>
									<?php /* 
											    $terms = get_the_terms( $post->ID , 'categories' );
												foreach ( $terms as $term ) {
													$link = get_term_link($term);
						               			?><br>
												<span class='post-labels'>
													<a href="<?php echo $link; ?>" id="cate-name-link"><i class="fa fa-folder-open-o"></i> <?php echo $term->name; ?></a>
												</span>
												<?php
												} */
													?>
								</center>
							</div>
							
						</div>
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');?>
						<div class="col-sm-6 no-padding blog-image" style="background-image: url('<?php echo $thumb['0']; ?>');background-repeat: no-repeat; background-size: cover;height: 400px; ">
							
							<div class="img-overflow">
								<div class="blog-arrow"></div>
								<!-- <a href="<?php the_permalink(); ?>">
						   			<img src="<?php echo $thumb['0']; ?>">
						   		</a> -->
							</div>
						</div>
					</div><br><br>

			

			<?php
			}
					      	 $i++;
				endwhile; 
			?>
			 <?php if ($loop->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
            	<div>
            		<p id="blog-prev">
            			<span>
                        	<?php echo get_previous_posts_link( '&laquo; Previous Page' ); // display newer posts link ?>
                        </span>
					</p>
					<p id="blog-next" style="float:right">
                        <span>
                        <?php echo get_next_posts_link( 'Next Page &raquo;', $query->max_num_pages ); // display older posts link ?>
                    	</span>
                    </p>
                </div>
        <?php } ?>
			<?php

			endif; 
				?>
			
		<!-- </div> -->
	</div>
</section>
		
<?php get_footer(); ?>